from wtforms.validators import ValidationError


class OptionalIf(object):
  """
  Make a field optional if the other field is provided
  :param fieldname
    The name of the other field
  :param message:
    Error message to raise in case of a validation error.
  """

  def __init__(self, fieldname, message=None):
    self.fieldname = fieldname
    self.message = message

  def __call__(self, form, field):
    try:
      other = form[self.fieldname]
    except KeyError:
      raise ValidationError(field.gettext(u"Invalid field name '%s'.") % self.fieldname)
    if not field.data and not other.data:
      if self.message is None:
        self.message = field.gettext(u'This field is required.')

      raise ValidationError(self.message)
