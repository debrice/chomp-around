from wtforms.fields import TextField

class ListTextField(TextField):
  """
  Special field that accepts a list of strings
  """
  coerce = unicode
  def process_data(self, value):
    try:
      self.data = list(self.coerce(v) for v in value)
    except (ValueError, TypeError):
      self.data = None

  def process_formdata(self, valuelist):
    try:
      self.data = list(self.coerce(x) for x in valuelist)
    except ValueError:
        raise ValueError(self.gettext(u'Invalid choice(s): one or more data '
                                       'inputs could not be coerced'))
