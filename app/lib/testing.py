from urlparse import urlparse

from flask.ext.testing import TestCase as _TestCase
from fixture import SQLAlchemyFixture
from app import db, create_app

class TestCase(_TestCase):
  """
  Handle fixture loading on top of flask test case
  """
  datasets = []
  data = None
  fixture = None
  env = None

  def tear_down(self):
    pass

  def set_up(self):
    pass

  def create_app(self):
    return create_app(config='config_test')

  def create_fixture(self):
    return SQLAlchemyFixture(session=self.session, env=self.env)

  def setUp(self):
    self.session = db.create_scoped_session({'autocommit':True})
    if self.datasets:
      self.fixture = self.create_fixture()
      db.create_all()
      self.data = self.fixture.data(*self.datasets)
      self.data.setup()
    self.set_up()

  def tearDown(self):
    if self.datasets:
      self.data.teardown()
    db.session.remove()
    db.drop_all()
    self.tear_down()

  def login(self, email=None, password='password', ID=None):
    if ID:
      email = ID.email
    return self.client.post('/identities/login/', data={
          'email': email, 'password': password})

  def logout(self):
    return self.client.get('/identities/logout/')

  def assertPath(self, response, path):
    self.assertTrue(urlparse(response.location).path, path)

  def assertExist(self, model, **kwargs):
    self.assertIsNotNone(model.query.filter_by(**kwargs).first())

  def assertNotExist(self, model, **kwargs):
    self.assertIsNone(model.query.filter_by(**kwargs).first())

  def last(self, model):
    return model.query.order_by(model.id.desc()).first()
