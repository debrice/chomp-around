from functools import wraps

from flask import jsonify

def remote(fn):
  """
  jsonify response
  """
  @wraps(fn)
  def decorated_view(*args, **kwargs):
    return jsonify(fn(*args, **kwargs))
  return decorated_view

