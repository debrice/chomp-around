from flask import render_template

from wtforms.widgets import html_params, SubmitInput, HTMLString
from jinja2 import Markup


class Button(SubmitInput):
  def __call__(self, field, **kwargs):
    value = kwargs.pop('value', field.data and u"y")
    id = kwargs.pop('id', field.id)
    hidden = ''
    kwargs['data-name'] = field.name

    if value:
      kwargs['class_'] = kwargs.get('class_', '') + ' active'
      hidden = "<input %s>" % html_params(name=field.name, id=id, value='y',
                                          type='hidden')

    return HTMLString(u'<button %(params)s>%(hidden)s%(label)s</button>' % {
      "hidden": hidden,
      "label": field.label.text,
      "params": html_params(**kwargs)
    })


def image_picker(field,  **kwargs):
  return Markup(render_template('images/picker_widget.html', field=field, **kwargs))


def auto_complete(field, **kwargs):
  value = ''
  multiple = field.type == "QuerySelectMultipleField"
  if field.data:
    if multiple:
      value = ",".join([str(x.id) for x in field.data])
    else:
      value = field.data.id

  # Mapping variable name as bootstrap uses - in attribute names which
  # is not a correct named variable format in python.
  for key in kwargs:
    if key.startswith("data_"):
      kwargs[key.replace("_","-")] = kwargs.pop(key)

  kwargs["class_"] = "ac_input %s " % kwargs.get("class_", "")

  return Markup("""
    <input type="text" id="id_%(field_id)s" data-value="%(value)s"
            autocomplete="off"
           data-name="%(name)s" data-multiple=%(multiple)s %(attr)s>
    <div id="id_%(field_id)s_container" class="ac_items" %(attr)s></div>""") % {
      'field_id': kwargs.pop('id', field.id) ,
      'value': value,
      'name': field.name,
      'multiple': multiple,
      'attr': Markup(html_params(**kwargs)),
   }
