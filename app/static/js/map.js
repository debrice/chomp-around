CA = CA || {}

CA.map = function(options){
  // default values
  options = $.extend({
    project: "EPSG:3857",
    zoom: 15,
    layers: [new OpenLayers.Layer.OSM('main')]
  }, options);

  document.getElementById(options.div).innerHTML = "";
  var map = new OpenLayers.Map(options);
  this.map = map;

  var markers = [];
  var marker_layer;

  // return point with the correct projection
  function point(lon, lat){
    return new OpenLayers.Geometry.Point(lon, lat)
      .transform('EPSG:4326', 'EPSG:3857');
  }

  // center the map
  this.center = center;
  function center(lon, lat, zoom){
    if (zoom){
      map.zoomTo(zoom);
    }
    map.setCenter(point(lon, lat).getBounds().getCenterLonLat());
  }

  this.clear_markers = clear_markers;
  function clear_markers(){
    marker_layer.removeAllFeatures();
  }

  // draw a marker on the map
  this.marker = marker;
  function marker(lon, lat){
    if (!marker_layer){
      marker_layer = new OpenLayers.Layer.Vector('marker');
      map.addLayer(marker_layer);
    }
    var feature = new OpenLayers.Feature.Vector(
     point(lon, lat), {}, {
       externalGraphic: '/static/assets/map_marker_48.png',
       graphicHeight: 48,
       graphicWidth: 48
     });
   marker_layer.addFeatures(feature);
   markers.push(feature);
  }

  this.geocode = geocode;
  function geocode(q, cb){
    jQuery.getJSON("http://nominatim.openstreetmap.org/search", q, cb);
  }
};
