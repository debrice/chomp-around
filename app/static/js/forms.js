CA.forms = {
  time_field: function(){
    var field = $(this).children('input');
    field.change(time_change);
    $(this).children('button').click(time_change);
    function time_change(){
      var hours, minutes;
      try{
        hours = parseInt(field.val().split(':')[0], 10);
        minutes = parseInt(field.val().split(':')[1], 10);
      }catch(err){
        hours = 0;
        minutes = 0;
      }

      hours = isNaN(hours) ? 0 : hours;
      minutes = isNaN(minutes) ? 0 : minutes;

      if( $(this).children('i').hasClass('icon-minus') ){
        hours = hours > 1 ? hours - 1 : 23;
      }else if( $(this).children('i').hasClass('icon-plus') ){
        hours = hours < 23 ? hours + 1 : 0;
      }
      hours = hours < 10 ? '0' + hours : hours;
      minutes = minutes < 10 ? '0' + minutes : minutes;
      field.val(hours + ':' + minutes);
    }
  },
  button_toggle: function(){
    var name = $(this).attr('data-name');
    $(this).click(function(event){
      event.preventDefault();
      if ($(this).hasClass('active')){
        $(this).children('input').remove()
      }else{
        $(this).prepend('<input type="hidden" name="'+ name +'" value="y">');
      }
    })
  }
}

$('.btn-group .btn').each(CA.forms.button_toggle);
$('.timefield').each(CA.forms.time_field);
