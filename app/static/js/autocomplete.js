CA.auto_completes = {};
CA.autoComplete = function(config){
  var suggest_message = " (hit enter to suggest it)";
  var values = config.value && config.value.split(',') || [];
  var items, ids = [];

  $('#' + config.id + '_container').click(function(){ $('#' + config.id).focus(); });

  this.init = init;
  function init(ids){
    var data = {all:1, ids: ids || config.value};
    values = ids && ids.split(',') || values;
    jQuery.getJSON(config.url, data, function(json){
      items = {};
      for(var elt in json) items[json[elt]]=elt;
      for(var x in values) updater(json[values[x]]);
    })
  }
  init();

  function source(query, callback){
    var data = {q: query, x: ids.join(',')};
    jQuery.getJSON(config.url, data, function(json){
      var elts = [];
      items = {};
      for (var elt in json){
        elts.push(json[elt]);
        items[json[elt]]=elt;
      }
      // if cannot find matching record then suggest creation
      if (elts.length == 0 && config.suggest){
        elts = [query + suggest_message];
      }
      callback(elts);
    });
  }

  function hideInput(){
    $('#' + config.id).hide();
  }

  function showInput(){
    $('#' + config.id).show();
  }

  function updater(item){
    var elt, id, name, suggest_class = '';

    if (item in items){
      id = items[item];
      name = config.name;
    }else if(config.suggest){
      // item is a suggestion, lets store it's value
      item = id = item.slice(0,item.length-suggest_message.length);
      name = config.name + '_suggest';
      suggest_class = 'ac_suggestion'
    }else{
      return;
    }

    // if item already been added ignore add request
    if (jQuery.inArray(id, ids) != -1) return;

    ids.push(id);

    // If autocomplete is not multiple we hide the field after the
    // first element get selected
    if (!config.multiple) hideInput();

    // called when newly added item gets deleted
    function removeItem(){
      ids.splice( $.inArray(id, ids), 1 );
      elt.remove();
      // Show back input when autocomplete is not multiple
      if (!config.multiple) showInput();
    }

    // generate the HTML element representing the added item
    elt = $('<span class="ac_item '+ suggest_class +'">' + item +
      '<input type="hidden" name="' + name + '" value="' + id  + '"></span>')
       .append($('<a class="ac_close">&times;</a>').click(removeItem))
       .appendTo('#' + config.id + '_container');
    if (config.callback in window) window[config.callback](id, item);
    // Clear the typeahead input field
    return "";
  }

  $('#' + config.id).typeahead({
    source: source,
    updater: updater
  });

};

$('.ac_input').each(function(elt){
  var id = this.getAttribute('id');
  CA.auto_completes[id] = new CA.autoComplete({
    id: id,
    url: this.getAttribute('data-url'),
    value: this.getAttribute('data-value'),
    multiple: this.getAttribute('data-multiple') == 'True',
    suggest: this.getAttribute('data-suggest') == 'True',
    name: this.getAttribute('data-name'),
    callback: this.getAttribute('data-callback')
  });
});
