from flask import Blueprint, request, render_template, redirect, url_for, flash
from flask.ext.login import login_required, current_user

from app import db
from app.lib.decorators import remote
from app.recipes.views import suggested_ingredient, suggested_recipe

from .forms import RestaurantForm, MenuForm, DishForm, PlaceForm, MenuDishForm,\
  FullDishForm, RestaurantPostForm, RestaurantPostCommentForm
from .models import Restaurant, Menu, Role, Dish, Place, MenuDish, \
  RestaurantPost, RestaurantPostComment
from .constants import OWNER, RESTAURANT


mod = Blueprint("restaurants", __name__, url_prefix="/restaurants")


def _create_restaurant(name, user, category_id=RESTAURANT):
  restaurant = Restaurant()
  restaurant.name = name
  restaurant.category_id = category_id
  db.session.add(restaurant)

  restaurant_user = Role()
  restaurant_user.restaurant = restaurant
  restaurant_user.user = user
  restaurant_user.role_id = OWNER
  db.session.add(restaurant_user)
  return restaurant


@mod.route("/<int:id>/post/", methods=["POST"])
@login_required
def create_post(id):
  restaurant = Restaurant.query.get_or_404(id)
  form = RestaurantPostForm()
  if form.validate_on_submit():
    post = RestaurantPost()
    post.restaurant = restaurant
    post.author = current_user.user
    post.content = form.content.data
    post.image = form.image.data
    db.session.add(post)
    db.session.commit()
    flash('Your post has been created', 'success')
  else:
    flash('An error occured during the creation of your post', 'error')

  return redirect(url_for("restaurants.view_restaurant", id=restaurant.id))


@mod.route("/<int:id>/post_comment/", methods=["POST"])
@login_required
def create_post_comment(id):
  post = RestaurantPost.query.get_or_404(id)
  form = RestaurantPostCommentForm()
  hashtag = ""
  if form.validate_on_submit():
    comment = RestaurantPostComment()
    comment.post = post
    comment.author = current_user.user
    comment.content = form.content.data
    db.session.add(comment)
    db.session.commit()
    flash('Your comment has been created', 'success')
    hashtag = "#comment_%s" % comment.id

  else:
    flash('An error occured during the creation of your comment', 'error')

  return redirect(
    url_for("restaurants.view_restaurant", id=post.restaurant.id) + hashtag)


@mod.route("/<int:id>/", methods=["GET"])
def view_restaurant(id):
  """
  public facing restaurant page
  """
  restaurant = Restaurant.query.get_or_404(id)
  kwargs = {
    "restaurant": restaurant,
    "posts": restaurant.posts[:10],
  }

  if current_user.is_authenticated():
    kwargs["post_form"] = RestaurantPostForm()
    kwargs["comment_form"] = RestaurantPostCommentForm()

  return render_template("restaurants/restaurant.html", **kwargs)


# first url only present to resolve purpose
@mod.route("/r/comments/", methods=["GET"])
@mod.route("/r/comments/<int:id>/", methods=["GET"])
def post_comments(id=0):
  """
  loads all the comments minus the last 2 ones for a given post
  """
  post = RestaurantPost.query.get_or_404(id)

  return render_template("restaurants/restaurant_comments.html",
    comments=reversed(post.comments[2:]))


@mod.route("/r/<int:id>/", methods=["GET"])
@mod.route("/r/<int:id>/<int:page>/", methods=["GET"])
def more_posts(id, page=1):
  """
  loads another set of post
  """
  restaurant = Restaurant.query.get_or_404(id)
  kwargs = {
    'posts': restaurant.posts.paginate(page, per_page=2)
  }

  if current_user.is_authenticated():
    kwargs["comment_form"] = RestaurantPostCommentForm()

  return render_template("restaurants/restaurant_posts.html", **kwargs)


@mod.route("/<int:id>/edit/", methods=["GET", "POST"])
@login_required
def edit_restaurant(id):
  """
  restaurant edit form
  """
  role = current_user.user.roles.\
    filter_by(role_id=OWNER, restaurant_id=id).first_or_404()
  restaurant = role.restaurant

  form = RestaurantForm(request.form, obj=restaurant)
  if form.validate_on_submit():
    restaurant.category_id = form.category_id.data
    restaurant.name = form.name.data
    db.session.commit()
    return redirect(url_for("restaurants.settings", id=restaurant.id))
  return render_template("restaurants/edit_restaurant.html", form=form,
    restaurant=restaurant)


@mod.route("/create/", methods=["GET", "POST"])
@login_required
def create_restaurant():
  """
  restaurant create form
  """
  form = RestaurantForm(request.form)
  if form.validate_on_submit():
    restaurant = _create_restaurant(
      name=form.name.data,
      user=current_user.user,
      category_id=form.category_id.data
    )
    db.session.add(restaurant)
    db.session.commit()
    return redirect(url_for("restaurants.settings", id=restaurant.id))
  return render_template("restaurants/create_restaurant.html", form=form)


@mod.route("/edit/place/<int:id>/", methods=["GET", "POST"])
@login_required
def edit_place(id):
  """
  Edit a place to be linked to the restaurant
  """
  place = Place.query.filter_by(id=id).first_or_404()

  role = current_user.user.roles.\
    filter_by(role_id=OWNER, restaurant_id=place.restaurant_id).first_or_404()
  restaurant = role.restaurant

  form = PlaceForm(obj=place)
  if form.validate_on_submit():
    place.name = form.name.data
    place.street = form.street.data
    place.city = form.city.data
    place.zipcode = form.zipcode.data
    place.state = form.state.data
    place.longitude = form.longitude.data
    place.latitude = form.latitude.data
    db.session.commit()
    flash("Place successfully saved", "success")
    return redirect(url_for("restaurants.settings", id=restaurant.id))

  return render_template("restaurants/edit_place.html", form=form,
    section="restaurant_%s" % restaurant.id, restaurant=restaurant)


@mod.route("/<int:id>/create/place/", methods=["GET", "POST"])
@login_required
def create_place(id):
  """
  Create a place to be linked to the restaurant
  """
  role = current_user.user.roles.\
    filter_by(role_id=OWNER, restaurant_id=id).first_or_404()
  restaurant = role.restaurant

  form = PlaceForm(restaurant_id=restaurant.id)
  if form.validate_on_submit():
    place = Place()
    place.name = form.name.data
    place.street = form.street.data
    place.city = form.city.data
    place.zipcode = form.zipcode.data
    place.state = form.state.data
    place.longitude = form.longitude.data
    place.latitude = form.latitude.data
    place.restaurant = restaurant
    db.session.add(place)
    db.session.commit()
    flash("Place successfully created", "success")
    return redirect(url_for("restaurants.settings", id=restaurant.id))

  return render_template("restaurants/create_place.html", form=form,
    section="restaurant_%s" % restaurant.id, restaurant=restaurant)


@mod.route("/<int:id>/create/menu/", methods=["GET", "POST"])
@login_required
def create_menu(id):
  """
  Create a menu to be linked to the restaurant
  id = restaurant id where the menu is available
  """
  role = current_user.user.roles.\
    filter_by(role_id=OWNER, restaurant_id=id).first_or_404()
  restaurant = role.restaurant

  form = MenuForm(restaurant_id=restaurant.id)
  if form.validate_on_submit():
    menu = Menu()
    menu.name = form.name.data
    menu.restaurant = restaurant
    db.session.add(menu)
    db.session.commit()
    flash("Menu successfully created", "success")
    return redirect(url_for("restaurants.edit_menu", id=menu.id))

  return render_template("restaurants/create_menu.html", form=form,
    restaurant=restaurant)


@mod.route("/menu/<int:id>/edit/", methods=["GET", "POST"])
@login_required
def edit_menu(id):
  """
  Edit a menu
  """
  menu = Menu.query.get_or_404(id)

  role = current_user.user.roles.\
    filter_by(role_id=OWNER, restaurant_id=menu.restaurant.id).first_or_404()
  restaurant = role.restaurant

  form = MenuForm(request.form, obj=menu)
  if form.validate_on_submit():
    menu.name = form.name.data
    menu.places = form.places.data
    menu.monday = form.monday.data
    menu.tuesday = form.tuesday.data
    menu.wednesday = form.wednesday.data
    menu.thursday = form.thursday.data
    menu.friday = form.friday.data
    menu.saturday = form.saturday.data
    menu.sunday = form.sunday.data
    menu.start = form.start.data.time()
    menu.stop = form.stop.data.time()
    db.session.commit()
    flash("Menu successfully saved.", "success")
    return redirect(url_for("restaurants.settings", id=restaurant.id))

  return render_template("restaurants/edit_menu.html", form=form,
    menu=menu, restaurant=restaurant,
    menu_dish_form=MenuDishForm(prefix="md_", menu_id=menu.id))


@mod.route("/menu/dish/delete/<int:id>/", methods=["POST"])
@login_required
def delete_menu_dish(id):
  menu_dish = MenuDish.query.get(id)

  current_user.user.roles.filter_by(restaurant_id=menu_dish.menu.restaurant_id,
                                    role_id=OWNER).first_or_404()

  db.session.delete(menu_dish)
  db.session.commit();
  flash("Dish %s has been removed from this menu." % menu_dish.name, "success")
  return redirect(url_for("restaurants.edit_menu", id=menu_dish.menu_id))


@mod.route("/dish/<int:id>/edit/", methods=["GET", "POST"])
@login_required
def edit_dish(id):
  """
  Edit a dish
  """
  dish = Dish.query.get_or_404(id)

  role = current_user.user.roles.\
    filter_by(role_id=OWNER, restaurant_id=dish.restaurant_id).first_or_404()
  restaurant = role.restaurant

  form = FullDishForm(request.form, obj=dish)
  if form.validate_on_submit():
    dish.name = form.name.data
    dish.description = form.description.data
    dish.category_id = form.category_id.data
    dish.hero_ingredient = form.hero_ingredient.data
    dish.ingredients = form.ingredients.data

    for ingredient in form.ingredients_suggest.data:
      ingredient = suggested_ingredient(ingredient)
      if ingredient:
        dish.ingredients.append(ingredient)

    if form.recipe_suggest.data:
      dish.recipe = suggested_recipe(form.recipe_suggest.data,
        category_id=form.category_id.data,
        ingredients = form.ingredients.data
      )
    else:
      dish.recipe = form.recipe.data

    dish.image = form.image.data
    dish.restaurant = restaurant
    db.session.commit()
    flash("Dish successfully saved", "success")
    return redirect(url_for("restaurants.settings", id=restaurant.id))

  return render_template("restaurants/edit_dish.html", form=form,
    restaurant=restaurant, dish=dish)


@mod.route("/<int:id>/dish/create/", methods=["GET", "POST"])
@login_required
def create_dish(id):
  """
  Create a dish
  """
  role = current_user.user.roles.\
    filter_by(role_id=OWNER, restaurant_id=id).first_or_404()
  restaurant = role.restaurant

  form = DishForm(request.form, restaurant_id=restaurant.id)
  if form.validate_on_submit():
    dish = Dish()
    dish.name = form.name.data
    dish.restaurant = restaurant
    db.session.add(dish)
    db.session.commit()
    flash("Dish successfully created", "success")
    return redirect(url_for("restaurants.edit_dish", id=dish.id))

  return render_template("restaurants/create_dish.html", form=form,
    restaurant=restaurant)


@mod.route("/<int:id>/settings/", methods=["GET", "POST"])
@login_required
def settings(id):
  role = current_user.user.roles.\
    filter_by(role_id=OWNER, restaurant_id=id).first_or_404()
  restaurant = role.restaurant
  menu_form = MenuForm(restaurant_id = restaurant.id)
  dish_form = DishForm(restaurant_id = restaurant.id)
  return render_template("restaurants/settings.html", restaurant=restaurant,
    section="restaurant_%s" % restaurant.id, menu_form=menu_form,
    dish_form=dish_form)


@mod.route("/menu/<int:id>/add_dish/", methods=["GET", "POST"])
@login_required
def add_menu_dish(id):
  menu = Menu.query.get_or_404(id)

  role = current_user.user.roles.\
    filter_by(role_id=OWNER, restaurant_id=menu.restaurant.id).first_or_404()

  form = MenuDishForm(menu_id=menu.id, prefix="md_")
  if form.validate_on_submit():
    menu_dish = MenuDish()
    menu_dish.name = form.name.data
    menu_dish.dishes = form.dishes.data
    menu_dish.price = form.price.data
    menu_dish.description = form.description.data
    menu_dish.menu_id = menu.id
    db.session.add(menu_dish)
    db.session.commit()
    flash("Dish successfully added to this menu", "success")
    return redirect(url_for("restaurants.edit_menu", id=menu.id))

  return render_template("restaurants/edit_menu.html", form=MenuForm(obj=menu),
    menu=menu, restaurant=role.restaurant, menu_dish_form=form)


@mod.route("/r/<int:id>/places/", methods=["GET"])
@login_required
@remote
def remote_places(id):
  args = []
  if "ids" in request.values:
    args.append(Place.id.in_(request.values["ids"].split(",")))
  if "q" in request.values:
    args.append(Place.name.like("%%%s%%" % request.values["q"]))
  if "x" in request.values:
    args.append(~Place.id.in_(request.values["x"].split(",")))

  data = Place.query.filter(Place.restaurant_id==id, *args).limit(20)
  return dict((x.id, x.name) for x in data)


@mod.route("/r/<int:id>/dishes/", methods=["GET"])
@login_required
@remote
def remote_dishes(id):
  args = []
  if "ids" in request.values:
    args.append(Dish.id.in_(request.values["ids"].split(",")))
  if "q" in request.values:
    args.append(Dish.name.like("%%%s%%" % request.values["q"]))
  if "x" in request.values:
    args.append(~Dish.id.in_(request.values["x"].split(",")))

  data = Dish.query.filter(Dish.restaurant_id==id, *args).limit(20)
  return dict((x.id, x.name) for x in data)


@mod.route("/r/recipe/", methods=["GET"])
@remote
def remote_dish():
  """
  return ingredients contained in a recipe.
  """
  id = request.args.get("id")
  dish = Dish.query.get_or_404(id)
  return {
    "description": dish.description,
    "category_id": dish.category_id,
    "name": dish.name,
  }
