from flask.ext.wtf import Form, RadioField, TextField, Required, TextArea, \
  Length, FloatField, QuerySelectMultipleField, Select, NumberRange, \
  HiddenInput, QuerySelectField, ValidationError, BooleanField, \
  SelectField, DateTimeField

from app.recipes.models import get_ingredients, get_recipes
from app.images.models import get_images
from app.recipes.constants import CATEGORIES
from app.lib.widgets import auto_complete, Button, image_picker
from app.lib.fields import ListTextField
from app.lib.validators import OptionalIf

import constants as RESTAURANT
from .models import Dish, Menu, MenuDish, Place, get_dishes, get_places


class RestaurantForm(Form):
  category_id = RadioField("Category", choices=RESTAURANT.CATEGORIES,
    coerce=int, widget=Select())
  name = TextField("Restaurant's name", [Required(), Length(min=3, max=255)])


class PlaceForm(Form):
  name = TextField("Name", [Required(), Length(max=255)])
  street = TextField("Street", [Required(), Length(max=1024)])
  zipcode = TextField("Zipcode", [Required(), Length(max=32)])
  city = TextField("City", [Required(), Length(max=255)])
  state = TextField("State", [Required(), Length(max=2)])
  blah = TextField("City",widget=HiddenInput)
  longitude = FloatField(validators=[Required(), NumberRange(min=-180, max=180)])
  latitude = FloatField(validators=[Required(), NumberRange(min=-90, max=90)])

  def __init__(self, *args, **kwargs):
    super(PlaceForm, self).__init__(*args, **kwargs)
    self.place = kwargs.get("obj")
    self.restaurant_id = self.place and self.place.restaurant_id or \
      kwargs.pop("restaurant_id")

  def validate_name(self, field):
    if self.place and self.place.name == field.data:
      return

    if Place.query.filter_by(name=field.data,
                            restaurant_id=self.restaurant_id).all():
      raise ValidationError("Place \"%s\" already exist." % field.data)


class MenuForm(Form):
  name = TextField("Name", [Required(), Length(max=255)])
  monday = BooleanField(widget=Button())
  tuesday = BooleanField("Tuesday", widget=Button())
  wednesday = BooleanField("Wednesday", widget=Button())
  thursday = BooleanField("Thursday", widget=Button())
  friday = BooleanField("Friday", widget=Button())
  saturday = BooleanField("Saturday", widget=Button())
  sunday = BooleanField("Sunday", widget=Button())
  start = DateTimeField("Starts", format="%H:%M")
  stop = DateTimeField("Stops", format="%H:%M")
  places = QuerySelectMultipleField(u"Serving places", get_label="name",
    allow_blank=True, query_factory=get_places, widget=auto_complete)

  def __init__(self, *args, **kwargs):
    super(MenuForm, self).__init__(*args, **kwargs)
    self.menu = kwargs.get("obj")
    self.restaurant_id = self.menu and self.menu.restaurant_id or \
      kwargs.pop("restaurant_id")

  def validate_name(self, field):
    if self.menu and self.menu.name == field.data:
      return

    if Menu.query.filter_by(name=field.data,
                            restaurant_id=self.restaurant_id).all():
      raise ValidationError("Menu \"%s\" already exist." % field.data)


class MenuDishForm(Form):
  price = FloatField("Price", [Required(), NumberRange(min=0)])
  name = TextField("Name", [Required(), Length(max=255)])
  category = SelectField(choices=CATEGORIES, coerce=int)
  description = TextField("Description", widget=TextArea())
  dishes = QuerySelectMultipleField("Dishes", [Required()], get_label="name",
    allow_blank=True, query_factory=get_dishes, widget=auto_complete)

  def __init__(self, *args, **kwargs):
    super(MenuDishForm, self).__init__(*args, **kwargs)
    self.menu_dish = kwargs.get("obj")
    self.menu_id = self.menu_dish and self.menu_dish.menu_id or kwargs.pop("menu_id")

  def validate_name(self, field):
    if self.menu_dish and self.menu_dish.name == field.data:
      return

    if MenuDish.query.filter_by(name=field.data, menu_id=self.menu_id).all():
      raise ValidationError("\"%s\" already exists in this menu." % field.data)

class DishForm(Form):
  name = TextField("Name", [Required(), Length(max=255)])

  def __init__(self, *args, **kwargs):
    super(DishForm, self).__init__(*args, **kwargs)
    self.dish = kwargs.get("obj")
    self.restaurant_id = self.dish and self.dish.restaurant_id or kwargs.pop("restaurant_id")

  def validate_name(self, field):
    if self.dish and self.dish.name == field.data:
      return

    if Dish.query.filter_by(name=field.data,
                            restaurant_id=self.restaurant_id).all():
      raise ValidationError("Dish \"%s\" already exist." % field.data)


class FullDishForm(DishForm):
  category_id = SelectField(choices=CATEGORIES, coerce=int)
  ingredients = QuerySelectMultipleField("Ingredients", [Required()],
    get_label="name", allow_blank=True, query_factory=get_ingredients,
    widget=auto_complete)
  hero_ingredient = QuerySelectField("Hero Ingredient", [Required()],
    get_label="name", allow_blank=True, query_factory=get_ingredients,
    widget=auto_complete)
  recipe = QuerySelectField("Recipe",  [OptionalIf('recipe_suggest')],
    get_label="name", allow_blank=True, query_factory=get_recipes,
    widget=auto_complete)
  description = TextField("Description", widget=TextArea())
  image = QuerySelectField("Update Image", get_label="file",
    allow_blank=True, query_factory=get_images, widget=image_picker)
  ingredients_suggest = ListTextField()
  recipe_suggest = TextField()


class RestaurantPostCommentForm(Form):
  content = TextField("Content", widget=TextArea())


class RestaurantPostForm(Form):
  content = TextField("Content", widget=TextArea())
  image = QuerySelectField("Add an Image", get_label="file",
    allow_blank=True, query_factory=get_images, widget=image_picker)
