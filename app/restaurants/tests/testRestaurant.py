from flask import url_for

from app.lib.testing import TestCase
from app.identities.tests import fixtures as identity_fixtures
from app.recipes.tests import fixtures as recipes_fixtures
from app.restaurants import constants as RESTAURANT
from app.restaurants.models import Restaurant, Role

from fixtures import all_data, env

env.update(identity_fixtures.env)
env.update(recipes_fixtures.env)

class TestRestaurant(TestCase):
  datasets = all_data + identity_fixtures.all_data + recipes_fixtures.all_data
  env = env

  # restaurant create data
  create_data = {
    "name": "Tamales 42",
    "category_id": RESTAURANT.RESTAURANT,
  }
  edit_data = {
    "name": "new name",
    "category_id": RESTAURANT.FOOD_TRUCK,
  }

  def set_up(self):
    self.identity = self.data.IdentityData.ActiveUser
    self.user = self.identity.user

  def test_create_requires_login(self):
    response = self.client.get(url_for("restaurants.create_restaurant"))
    self.assertStatus(response, 302)
    self.assertPath(response, url_for('identities.login'))

  def test_create_restaurant(self):
    create_URL = url_for("restaurants.create_restaurant")
    response = self.login(ID=self.identity)

    # simple rendering, ensure template exists, and render correctly
    self.client.get(create_URL)

    response = self.client.post(create_URL, data=self.create_data)

    # record should exist
    restaurant = Restaurant.query.filter_by(name=self.create_data["name"])\
      .first()

    self.assertEqual(restaurant.category_id, self.create_data["category_id"])
    self.assertEqual(restaurant.name, self.create_data["name"])

    # creation should redirect to settings page of the created restaurant
    self.assertRedirects(response, url_for("restaurants.settings",
                                           id=restaurant.id))

    # User should be an admin
    self.assertExist(Role, user_id=self.user.id,
      role_id=RESTAURANT.OWNER,
      restaurant_id=restaurant.id)


  def test_edit_restaurant(self):
    edit_URL = url_for("restaurants.edit_restaurant",
                       id=self.data.RestaurantData.Pizza101.id)
    response = self.login(ID=self.data.IdentityData.ActiveUser)

    # ensure templates is there and renders correctly
    self.client.get(edit_URL)

    response = self.client.post(edit_URL, data=self.edit_data)

    # creation should redirect to settings page of the created restaurant
    self.assertRedirects(response,
                         url_for("restaurants.settings",
                                 id=self.data.RestaurantData.Pizza101.id))

    # modified record should exist
    self.assertExist(Restaurant, **self.edit_data)

  def test_cannot_edit_other_restaurant(self):
    edit_URL = url_for("restaurants.edit_restaurant",
                       id=self.data.RestaurantData.BurittoPlace.id)

    response = self.login(ID=self.data.IdentityData.ActiveUser)
    # posting and getting edit should raise a 404 as this user isn't
    # owner of this place
    response = self.client.get(edit_URL)
    self.assertStatus(response, 404)

    response = self.client.post(edit_URL, data=self.edit_data)
    self.assertStatus(response, 404)

    # Ensure restaurant didn't get modified
    self.assertNotExist(Restaurant, name=self.edit_data["name"])
