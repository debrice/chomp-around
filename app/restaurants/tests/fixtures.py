from datetime import time

from fixture import DataSet

from app.identities.tests.fixtures import UserData
from app.recipes.tests.fixtures import RecipeData, IngredientData2,\
  IngredientData

from ..models import Restaurant, Menu, Dish, MenuDish, Place, Role
from ..constants import RESTAURANT, OWNER


class RestaurantData(DataSet):
  class Pizza101:
    name = "pizza 101"
    category_id = RESTAURANT

  class BurittoPlace:
    name = "Buritto Place"
    category_id = RESTAURANT


class RoleData(DataSet):
  class SteveOwner:
    restaurant = RestaurantData.Pizza101
    user = UserData.Steve
    role_id = OWNER

  class RogerOwner:
    restaurant = RestaurantData.BurittoPlace
    user = UserData.Roger
    role_id = OWNER


class DishData(DataSet):
  class PizzaMargarita:
    restaurant = RestaurantData.Pizza101
    name = "Pizza Margarita facon"
    recipe = RecipeData.PizzaMargarita
    ingredients = [IngredientData2.PizzaDoe, IngredientData.Mozarella,
      IngredientData.Morglub, # suggested ingredient
      IngredientData.Oregano, IngredientData.Tomatoe, IngredientData.Parmigiano]

  class Lasagna:
    restaurant = RestaurantData.Pizza101
    name = "lasagna du chef"
    recipe = RecipeData.Lasagna
    ingredients = [IngredientData2.Pasta, IngredientData.Tomatoe,
      IngredientData.Basil, IngredientData.Mozarella, IngredientData.Parmigiano]

  class Morglub:
    restaurant = RestaurantData.Pizza101
    name = "Morglub du chef"
    recipe = RecipeData.Morglub
    ingredients = [IngredientData.Butter, IngredientData.Mozarella]


class PlaceData(DataSet):
  class DownTown:
    name = "Downtown"
    street = "553 South Olive Ave"
    zipcode = "90013"
    city = "Los Angeles"
    latitude = 34.044940
    longitude = -118.255250
    restaurant = RestaurantData.Pizza101

  class Mall:
    name = "Mall"
    street = "200th Fourth Street"
    zipcode = "90013"
    city = "Los Angeles"
    latitude = 32.022000
    longitude = -117.000000
    restaurant = RestaurantData.BurittoPlace


class MenuData(DataSet):
  class Breakfast:
    name = 'Breakfast Menu'
    place = PlaceData.DownTown
    restaurant = RestaurantData.Pizza101
    start = time(8,00)
    stop = time(11, 00)
    monday = True
    tuesday = True
    wednesday = True
    thursday = True
    friday = True
    saturday = False
    sunday = False

  class Brunch:
    name = 'Brunch Menu'
    place = PlaceData.DownTown
    restaurant = RestaurantData.Pizza101
    description = "serving from 9am to 3pm every weekend"
    start = time(9,00)
    stop = time(15, 00)
    monday = False
    tuesday = False
    wednesday = False
    thursday = False
    friday = False
    saturday = True
    sunday = True

  class Dinner:
    name = "Dinner Menu"
    place = PlaceData.Mall
    restaurant = RestaurantData.BurittoPlace
    description = "serving from for dinner"
    start = time(16,00)
    stop = time(23, 00)
    monday = False
    tuesday = False
    wednesday = False
    thursday = False
    friday = False
    saturday = True
    sunday = True


class MenuDishData(DataSet):
  class PizzaMargarita:
    dish = DishData.PizzaMargarita
    price = 55.3
    menu = MenuData.Breakfast


all_data = [
  RestaurantData,
  RoleData,
  DishData,
  PlaceData,
  MenuData,
  MenuDishData,
]

env = {
  "RoleData": Role,
  "RestaurantData": Restaurant,
  "MenuData": Menu,
  "PlaceData": Place,
  "DishData": Dish,
  "MenuDishData": MenuDish,
}
