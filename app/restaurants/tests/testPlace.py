from app.lib.testing import TestCase

from app.identities.tests import fixtures as identity_fixtures
from fixtures import all_data, env

env.update(identity_fixtures.env)


class TestPlace(TestCase):
  datasets = all_data + identity_fixtures.all_data
  env = env

  create_data = {
    "name": "Le Place",
    "street": "455 5th street",
    "zipcode": "90999",
    "state": "CA",
    "city": "Los Anageles",
  }
