from flask import url_for

from app.lib.testing import TestCase

from app.identities.tests import fixtures as identity_fixtures
from app.recipes.tests import fixtures as recipes_fixtures

from ..constants import OWNER
from ..models import Menu

from fixtures import all_data, env

env.update(identity_fixtures.env)
env.update(recipes_fixtures.env)


class TestMenu(TestCase):
  datasets = all_data + identity_fixtures.all_data + recipes_fixtures.all_data
  env = env

  create_data = {
    "name": "Le Menu",
    "monday": "y",
    "wednesday": "y",
    "friday": "y",
    "start": "09:22",
    "stop": "19:22",
  }

  def set_up(self):
    self.identity = self.data.IdentityData.ActiveUser
    self.restaurant = self.data.UserData.Steve.roles.filter_by(role_id=OWNER)\
      .first().restaurant

  def test_create_requires_login(self):
    createURL = url_for("restaurants.create_menu", id=self.restaurant.id)
    response = self.client.get(createURL)
    self.assertStatus(response, 302)
    self.assertPath(response, url_for('identities.login'))

  def test_create_and_edit_menu(self):
    createURL = url_for("restaurants.create_menu", id=self.restaurant.id)
    response = self.login(ID=self.identity)
    response = self.client.post(createURL,
      data={'name': self.create_data['name']})
    self.assertStatus(response, 302)

    # assert menu gets created
    menu = Menu.query.filter_by(restaurant_id=self.restaurant.id,
      name=self.create_data['name']).first()

    # now will add the serving days and hours to it
    editURL = url_for("restaurants.edit_menu", id=menu.id)
    response = self.client.post(editURL, data=self.create_data)
    self.assertStatus(response, 302)
    self.assertTrue(all((menu.monday, menu.wednesday, menu.friday)))
    self.assertFalse(any((menu.tuesday, menu.thursday, menu.saturday,
                         menu.sunday)))

  def test_cannot_create_menu_on_other_restaurant(self):
    role = self.data.UserData.Roger.roles.filter_by(role_id=OWNER).first()
    restaurant = role.restaurant
    createURL = url_for("restaurants.create_menu", id=restaurant.id)

    response = self.login(ID=self.identity)
    response = self.client.get(createURL)
    self.assertStatus(response, 404)

    response = self.client.post(createURL, data=self.create_data)
    self.assertStatus(response, 404)

  def test_cannot_edit_menu_on_other_restaurant(self):
    role = self.data.UserData.Roger.roles.filter_by(role_id=OWNER).first()
    restaurant = role.restaurant
    menu = role.restaurant.menus.first()
    editURL = "/restaurants/%d/edit/menu/%d/" % (restaurant.id, menu.id)

    response = self.login(ID=self.identity)
    response = self.client.get(editURL)
    self.assertStatus(response, 404)

    response = self.client.post(editURL, data=self.create_data)
    self.assertStatus(response, 404)

    # ensure restaurant didn't update
    self.assertIsNotNone(Menu.query.filter_by(name=menu.name).first())
