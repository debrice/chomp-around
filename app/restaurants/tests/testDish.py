from flask import url_for

from app.lib.testing import TestCase
from app.identities.tests import fixtures as identity_fixtures
from app.recipes.constants import ENTREE, DESSERT, SUGGESTION
from app.recipes.tests import fixtures as recipes_fixtures
from app.recipes.models import Recipe, Ingredient
from app.restaurants.models import Dish
from app.restaurants.constants import OWNER

from .fixtures import all_data, env

env.update(identity_fixtures.env)
env.update(recipes_fixtures.env)


class TestDish(TestCase):
  # loads restaurant, identity and recipe fixtures
  datasets = all_data + identity_fixtures.all_data + recipes_fixtures.all_data
  env = env

  def set_up(self):
    self.identity = self.data.IdentityData.ActiveUser
    self.restaurant = self.data.UserData.Steve.roles.filter_by(role_id=OWNER)\
      .first().restaurant

  def test_suggestions_on_dish(self):
    recipe_suggestion = "lasagna suggestion"
    ingredient_suggestions = ["-foo", "-bar"]
    edit_URL = url_for("restaurants.edit_dish",
                       id=self.data.DishData.PizzaMargarita.id)
    response = self.login(ID=self.identity)

    # should be able to get
    response = self.client.get(edit_URL)
    self.assertStatus(response, 200)

    ingredients = [self.data.IngredientData.Butter,
      self.data.IngredientData.Egg]
    hero = ingredients[0]
    response = self.client.post(edit_URL, data={
      "name": "Lasagna mama",
      "category_id": DESSERT,
      "ingredients": [i.id for i in ingredients],
      "hero_ingredient": hero.id,
      "recipe_suggest": recipe_suggestion,
      "ingredients_suggest": ingredient_suggestions,
    })

    # after edit should get redirected to settings page of dish's restaurant
    self.assertRedirects(response, url_for("restaurants.settings",
                           id=self.data.DishData.PizzaMargarita.restaurant_id))

    # assert dish gets updated
    dish = Dish.query.filter_by(name="Lasagna mama").first()
    self.assertIsNotNone(dish)

    # recipe is created as suggestion
    recipe = Recipe.query.filter_by(name=recipe_suggestion).first()
    self.assertIsNotNone(recipe)
    for ingredient in ingredients:
      self.assertIn(ingredient.id, [i.id for i in recipe.ingredients])
    self.assertEqual(recipe.status_id, SUGGESTION)

    for ingredient in ingredient_suggestions:
      self.assertExist(Ingredient, name=ingredient, status_id=SUGGESTION)


  def test_edit_dish(self):
    edit_URL = url_for("restaurants.edit_dish",
                       id=self.data.DishData.PizzaMargarita.id)
    response = self.login(ID=self.identity)

    # should be able to get
    response = self.client.get(edit_URL)
    self.assertStatus(response, 200)

    ingredients = [self.data.IngredientData2.Pasta,
      self.data.IngredientData.Tomatoe]
    hero = ingredients[0]
    response = self.client.post(edit_URL, data={
      "name": "Lasagna mama",
      "category_id": ENTREE,
      "ingredients": [i.id for i in ingredients],
      "hero_ingredient": hero.id,
      "recipe": self.data.RecipeData.Lasagna.id,
      "description": "Le delicious dish",
    })

    # after edit should get redirected to settings page of dish's restaurant
    self.assertRedirects(response, url_for("restaurants.settings",
                           id=self.data.DishData.PizzaMargarita.restaurant_id))

    # assert dish gets updated
    dish = Dish.query.filter_by(name="Lasagna mama").first()
    self.assertIsNotNone(dish)

    # restaurant is still linked to the updated dish
    self.assertEqual(dish.restaurant_id, self.restaurant.id)

    # recipe is correctly updated
    self.assertEqual(dish.recipe_id, self.data.RecipeData.Lasagna.id)

    # dish contains the ingredients
    self.assertEqual(len(dish.ingredients), 2)

    # each ingredients are present
    for ingredient in dish.ingredients:
      self.assertIn(ingredient.name, [i.name for i in ingredients])
      self.assertIn(ingredient.id, [i.id for i in ingredients])

    # ingredient hero is right
    self.assertEqual(dish.hero_ingredient.id, hero.id)


  def test_create_dish(self):
    create_URL = url_for("restaurants.create_dish", id=self.restaurant.id)
    response = self.login(ID=self.identity)

    # should be able to get
    response = self.client.get(create_URL)
    self.assertStatus(response, 200)

    response = self.client.post(create_URL, data={
      "name": "Lasagna mama",
    })
    self.assertStatus(response, 302)

    # assert dish gets created
    dish = Dish.query.filter_by(name="Lasagna mama").first()
    self.assertIsNotNone(dish)

    # restaurant is linked to the created dish
    self.assertEqual(dish.restaurant_id, self.restaurant.id)
