from datetime import datetime
from app import db

import constants as RESTAURANT

from app.events.models import HasEvent
from app.recipes.constants import CATEGORIES, ENTREE


menu_places = db.Table("restaurants_menu_places",
  db.Column("place_id", db.Integer, db.ForeignKey("restaurants_place.id")),
  db.Column("menu_id", db.Integer, db.ForeignKey("restaurants_menu.id")),
)


ingredients_dishes = db.Table("restaurants_dish_ingredients",
  db.Column("dish_id", db.Integer, db.ForeignKey("restaurants_dish.id")),
  db.Column("ingredient_id", db.Integer, db.ForeignKey("recipes_ingredient.id")),
)


menu_dishes = db.Table("restaurants_menu_dishes",
  db.Column("dish_id", db.Integer, db.ForeignKey("restaurants_dish.id")),
  db.Column("menu_id", db.Integer, db.ForeignKey("restaurants_menu_dish.id")),
)


class Restaurant(db.Model):
  __tablename__ = "restaurants_restaurant"

  id = db.Column(db.Integer, primary_key=True)
  category_id = db.Column(db.SmallInteger, default=RESTAURANT.RESTAURANT)
  name = db.Column(db.String(255))

  users = db.relationship("Role")

  @property
  def category(self):
    return dict(RESTAURANT.CATEGORIES)[self.category]


class RestaurantPost(HasEvent, db.Model):
  __tablename__ = "restaurants_post"

  id = db.Column(db.Integer, primary_key=True)
  restaurant_id = db.Column(db.Integer, db.ForeignKey("restaurants_restaurant.id"))
  author_id = db.Column(db.Integer, db.ForeignKey("identities_user.id"))
  image_id = db.Column(db.Integer, db.ForeignKey("images_image.id"))
  creation_date = db.Column(db.DateTime, default=datetime.utcnow)
  content = db.Column(db.Text)
  public = db.Column(db.Boolean, default=True)

  author = db.relationship("User",
    backref=db.backref("restaurant_posts", lazy="dynamic", order_by=id.desc()))
  restaurant = db.relationship("Restaurant",
    backref=db.backref("posts", lazy="dynamic", order_by=id.desc()))
  image = db.relationship("Image",
    backref=db.backref("restaurant_posts", lazy="dynamic"))


class RestaurantPostComment(HasEvent, db.Model):
  __tablename__ = "restaurants_post_comment"

  id = db.Column(db.Integer, primary_key=True)
  post_id = db.Column(db.Integer, db.ForeignKey("restaurants_post.id"))
  author_id = db.Column(db.Integer, db.ForeignKey("identities_user.id"))
  creation_date = db.Column(db.DateTime, default=datetime.utcnow)
  content = db.Column(db.Text)

  post = db.relationship("RestaurantPost",
    backref=db.backref("comments", lazy="dynamic", order_by=id.desc()))
  author = db.relationship("User",
    backref=db.backref("restaurant_post_comments", lazy="dynamic",
                       order_by=id.desc()))


class Role(db.Model):
  __tablename__ = "restaurants_role"
  __table_args__ = (
    db.UniqueConstraint("user_id", "restaurant_id"),
  )

  user_id = db.Column(db.Integer, db.ForeignKey("identities_user.id"), primary_key=True)
  restaurant_id = db.Column(db.Integer, db.ForeignKey("restaurants_restaurant.id"),
    primary_key=True)

  role_id = db.Column(db.SmallInteger, default=RESTAURANT.ASSOCIATE)
  restaurant = db.relationship("Restaurant", backref=db.backref("roles", lazy="dynamic"))
  user = db.relationship("User", backref=db.backref("roles", lazy="dynamic",
    cascade="all, delete-orphan"))

  @property
  def role(self):
    return dict(RESTAURANT.ROLE_CHOICES)[self.role_id]


class MenuDish(db.Model):
  __tablename__ = "restaurants_menu_dish"
  __table_args__ = (
    db.UniqueConstraint("name", "menu_id"),
  )

  id = db.Column(db.Integer, primary_key=True)
  menu_id = db.Column(db.Integer, db.ForeignKey("restaurants_menu.id"))

  description = db.Column(db.Text, default="")
  name = db.Column(db.String(255))
  price = db.Column(db.Float())
  menu = db.relationship("Menu")
  dishes = db.relationship("Dish", secondary=menu_dishes,
          backref=db.backref("menu_dishes", lazy="dynamic"))


class Dish(db.Model):
  __tablename__ = "restaurants_dish"
  __table_args__ = (
    db.UniqueConstraint("name", "restaurant_id"),
  )

  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(255))
  description = db.Column(db.Text, default="")
  category_id = db.Column(db.SmallInteger, default=ENTREE)

  recipe_id = db.Column(db.Integer, db.ForeignKey("recipes_recipe.id"))
  hero_ingredient_id = db.Column(db.Integer,
                                 db.ForeignKey("recipes_ingredient.id"))
  restaurant_id = db.Column(db.Integer,
                            db.ForeignKey("restaurants_restaurant.id"))
  image_id = db.Column(db.Integer,
                       db.ForeignKey("images_image.id"))

  ingredients = db.relationship("Ingredient", secondary=ingredients_dishes,
                                backref=db.backref("dishes", lazy="dynamic"))
  restaurant = db.relationship("Restaurant",
                               backref=db.backref("dishes", lazy="dynamic"))
  hero_ingredient = db.relationship("Ingredient",
                            backref=db.backref("hero_dishes", lazy="dynamic"))
  recipe = db.relationship("Recipe",
                           backref=db.backref("dishes", lazy="dynamic"))
  image = db.relationship("Image",
                           backref=db.backref("dishes", lazy="dynamic"))

  @property
  def category(self):
    return dict(CATEGORIES)[self.category_id]


class Menu(db.Model):
  __tablename__ = "restaurants_menu"
  __table_args__ = (
    db.UniqueConstraint("name", "restaurant_id"),
  )

  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(255))

  # serving hours and days
  start = db.Column(db.Time)
  stop = db.Column(db.Time)
  monday = db.Column(db.Boolean)
  tuesday = db.Column(db.Boolean)
  wednesday = db.Column(db.Boolean)
  thursday = db.Column(db.Boolean)
  friday = db.Column(db.Boolean)
  saturday = db.Column(db.Boolean)
  sunday = db.Column(db.Boolean)

  restaurant_id = db.Column(db.Integer, db.ForeignKey("restaurants_restaurant.id"))
  restaurant = db.relationship("Restaurant", backref=db.backref("menus", lazy="dynamic"))
  places = db.relationship("Place", secondary=menu_places,
          backref=db.backref("menus", lazy="dynamic"))
  dishes = db.relationship("MenuDish",
          backref=db.backref("menus", lazy="dynamic"))

  @property
  def serving_days(self):
    """
    Returns human formated string of serving days, like:
    - "weekend only"
    - "Mon, Tue"
    """
    weekend = [self.sunday, self.saturday]
    weekday = [self.monday, self.tuesday, self.wednesday, self.thursday, self.friday]
    week = weekday + weekend
    if all(week):
      return "All week"
    elif all(weekend) and not any(weekday):
      return "Weekend only"
    elif all(weekday) and not any(weekend):
      return "Week days only"
    else:
      weekdays = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
      res = [weekdays[pos] for pos, day in enumerate(week) if day]
      return ", ".join(res)


  def __repr__(self):
    return self.name


class Place(db.Model):
  __tablename__ = "restaurants_place"
  __table_args__ = (
    db.UniqueConstraint("name", "restaurant_id"),
  )

  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(255))
  street = db.Column(db.String(1024))
  zipcode = db.Column(db.String(32), index=True)
  city = db.Column(db.String(255))
  state = db.Column(db.String(8))
  latitude = db.Column(db.Float(), index=True)
  longitude = db.Column(db.Float(), index=True)

  restaurant_id = db.Column(db.Integer, db.ForeignKey("restaurants_restaurant.id"))
  restaurant = db.relationship("Restaurant", backref=db.backref("places", lazy="dynamic"))


def get_dishes():
  #TODO: limit to the dishe from the current restaurant
  return Dish.query.filter()


def get_places():
  #TODO: limit to the places within the current restaurant
  return Place.query.filter()
