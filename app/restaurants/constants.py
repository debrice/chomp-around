FOOD_TRUCK = 0
RESTAURANT = 1

CATEGORIES = (
  (RESTAURANT, 'restaurant'),
  (FOOD_TRUCK, 'food truck'),
)

OWNER = 0
ASSOCIATE = 1

ROLE_CHOICES = {
  OWNER: 'owner',
  ASSOCIATE: 'associate',
}
