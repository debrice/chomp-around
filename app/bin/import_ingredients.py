from app import db
from app.recipes.models import Ingredient, IngredientCategory

from .ingredients import ingredients

def cleanup(text):
  return text.lower().strip()

def create_ingredient(name):
  ingredient = Ingredient()
  ingredient.name = name
  db.session.add(ingredient)
  db.session.commit()
  return ingredient

def create_ingredient_category(name):
  category = IngredientCategory()
  category.name = name
  db.session.add(category)
  db.session.commit()
  return category

def get_or_create_ingredient(name):
  name = cleanup(name)
  ingredient = Ingredient.query.filter_by(name=name).first()
  if ingredient is None:
    ingredient = create_ingredient(name)
  return ingredient

def get_or_create_category(name):
  name = cleanup(name)
  category = IngredientCategory.query.filter_by(name=name).first()
  if category is None:
    category = create_ingredient_category(name)
  return category

def import_ingredient(name, categories, parent=None):
  # list of ingredient
  if isinstance(name, tuple):
    ingredient = import_ingredient(name[0], categories, parent)
    # does it have children?
    if len(name) > 1:
      for sub_name in name[1]:
        import_ingredient(sub_name, categories, parent=ingredient)
  else:
    ingredient = get_or_create_ingredient(name)

  if parent is not None and parent not in ingredient.parents:
    ingredient.parents.append(parent)

  for category in categories:
    if category not in ingredient.categories:
      ingredient.categories.append(category)

  db.session.commit()
  return ingredient

def run():
  for categories, ingredient_names in ingredients.items():
    cats = [get_or_create_category(name) for name in categories.split(",")]
    for ingredient_name in ingredient_names:
      import_ingredient(ingredient_name, cats)
