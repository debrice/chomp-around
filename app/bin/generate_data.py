import random
from string import ascii_lowercase
from datetime import time
from werkzeug import FileStorage

from app import db, photos
from app.identities.views import _createUser
from app.recipes.models import Ingredient, Recipe
from app.recipes.constants import CATEGORIES
from app.restaurants.views import _create_restaurant
from app.restaurants.models import Place, Dish, Menu, MenuDish, RestaurantPost, \
  RestaurantPostComment
from app.images.models import Image

INGREDIENTS = Ingredient.query.all()

FIRST_NAMES = (
"JAMES", "JOHN", "ROBERT", "MICHAEL", "WILLIAM", "DAVID", "RICHARD", "CHARLES", "JOSEPH",
"THOMAS", "CHRISTOPHER", "DANIEL", "PAUL", "MARK", "DONALD", "GEORGE", "KENNETH",
"STEVEN", "EDWARD", "BRIAN", "RONALD", "ANTHONY", "KEVIN", "JASON", "MATTHEW", "GARY",
"TIMOTHY", "JOSE", "LARRY", "JEFFREY", "FRANK", "SCOTT", "ERIC", "STEPHEN", "ANDREW",
"RAYMOND", "GREGORY", "JOSHUA", "JERRY", "DENNIS", "WALTER", "PATRICK", "PETER", "HAROLD",
"DOUGLAS", "HENRY", "CARL", "ARTHUR", "RYAN", "ROGER", "JOE", "JUAN", "JACK", "ALBERT",
"JONATHAN", "JUSTIN", "TERRY", "GERALD", "KEITH", "SAMUEL", "WILLIE", "RALPH", "LAWRENCE",
"NICHOLAS", "ROY", "BENJAMIN", "BRUCE", "BRANDON", "ADAM", "HARRY", "FRED", "WAYNE",
"BILLY", "STEVE", "LOUIS", "JEREMY", "AARON", "RANDY", "HOWARD", "EUGENE", "CARLOS",
"RUSSELL", "BOBBY", "VICTOR", "MARTIN", "ERNEST", "PHILLIP", "TODD", "JESSE", "CRAIG",
"ALAN", "SHAWN", "CLARENCE", "SEAN", "PHILIP", "CHRIS", "JOHNNY", "EARL", "JIMMY",
"ANTONIO", "DANNY", "BRYAN", "TONY", "LUIS", "MIKE", "STANLEY", "LEONARD", "NATHAN",
"DALE", "MANUEL", "RODNEY", "CURTIS", "NORMAN", "ALLEN", "MARVIN", "VINCENT", "GLENN",
"JEFFERY", "TRAVIS", "JEFF", "CHAD", "JACOB", "LEE", "MELVIN", "ALFRED", "KYLE",
"FRANCIS", "BRADLEY", "JESUS", "HERBERT", "FREDERICK", "RAY", "JOEL", "EDWIN", "DON",
"EDDIE", "RICKY", "TROY", "RANDALL", "BARRY", "ALEXANDER", "BERNARD", "MARIO", "LEROY",
"FRANCISCO", "MARCUS", "MICHEAL", "THEODORE", "CLIFFORD", "MIGUEL", "OSCAR", "JAY", "JIM",
"TOM", "CALVIN", "ALEX", "JON", "RONNIE", "BILL", "LLOYD", "TOMMY", "LEON", "DEREK",
"WARREN", "DARRELL", "JEROME", "FLOYD", "LEO", "ALVIN", "TIM", "WESLEY", "GORDON", "DEAN",
"GREG", "JORGE", "DUSTIN", "PEDRO", "DERRICK", "DAN", "LEWIS", "ZACHARY", "COREY",
"HERMAN", "MAURICE", "VERNON", "ROBERTO", "CLYDE", "GLEN", "HECTOR", "SHANE", "RICARDO",
"SAM", "RICK", "LESTER", "BRENT", "RAMON", "CHARLIE", "TYLER", "GILBERT", "GENE", "MARC",
"REGINALD", "RUBEN", "BRETT", "ANGEL", "NATHANIEL", "RAFAEL", "LESLIE", "EDGAR", "MILTON",
"RAUL", "BEN", "CHESTER", "CECIL", "DUANE", "FRANKLIN", "ANDRE", "ELMER", "BRAD",
"GABRIEL", "RON", "MITCHELL", "ROLAND", "ARNOLD", "HARVEY", "JARED", "ADRIAN", "KARL",
"CORY", "CLAUDE", "ERIK", "DARRYL", "JAMIE", "NEIL", "JESSIE", "CHRISTIAN", "JAVIER",
"FERNANDO", "CLINTON", "TED", "MATHEW", "TYRONE", "DARREN", "LONNIE", "LANCE", "CODY",
"JULIO", "KELLY", "KURT", "ALLAN", "NELSON", "GUY", "CLAYTON", "HUGH", "MAX", "DWAYNE",
"DWIGHT", "ARMANDO", "FELIX", "JIMMIE", "EVERETT", "JORDAN", "IAN", "WALLACE", "KEN",
"BOB", "JAIME", "CASEY", "ALFREDO", "ALBERTO", "DAVE", "IVAN", "JOHNNIE", "SIDNEY",
"BYRON", "JULIAN", "ISAAC", "MORRIS", "CLIFTON", "WILLARD", "DARYL", "ROSS", "VIRGIL",
"ANDY", "MARSHALL", "SALVADOR", "PERRY", "KIRK", "SERGIO", "MARION", "TRACY", "SETH",
"KENT", "TERRANCE", "RENE", "EDUARDO", "TERRENCE", "ENRIQUE", "FREDDIE", "WADE", "MARY",
"PATRICIA", "LINDA", "BARBARA", "ELIZABETH", "JENNIFER", "MARIA", "SUSAN", "MARGARET",
"DOROTHY", "LISA", "NANCY", "KAREN", "BETTY", "HELEN", "SANDRA", "DONNA", "CAROL", "RUTH",
"SHARON", "MICHELLE", "LAURA", "SARAH", "KIMBERLY", "DEBORAH", "JESSICA", "SHIRLEY",
"CYNTHIA", "ANGELA", "MELISSA", "BRENDA", "AMY", "ANNA", "REBECCA", "VIRGINIA",
"KATHLEEN", "PAMELA", "MARTHA", "DEBRA", "AMANDA", "STEPHANIE", "CAROLYN", "CHRISTINE",
"MARIE", "JANET", "CATHERINE", "FRANCES", "ANN", "JOYCE", "DIANE", "ALICE", "JULIE",
"HEATHER", "TERESA", "DORIS", "GLORIA", "EVELYN", "JEAN", "CHERYL", "MILDRED",
"KATHERINE", "JOAN", "ASHLEY", "JUDITH", "ROSE", "JANICE", "KELLY", "NICOLE", "JUDY",
"CHRISTINA", "KATHY")

LAST_NAMES = (
"SMITH", "JOHNSON", "WILLIAMS", "JONES", "BROWN", "DAVIS", "MILLER", "WILSON", "MOORE",
"TAYLOR", "ANDERSON", "THOMAS", "JACKSON", "WHITE", "HARRIS", "MARTIN", "THOMPSON",
"GARCIA", "MARTINEZ", "ROBINSON", "CLARK", "RODRIGUEZ", "LEWIS", "LEE", "WALKER", "HALL",
"ALLEN", "YOUNG", "HERNANDEZ", "KING", "WRIGHT", "LOPEZ", "HILL", "SCOTT", "GREEN",
"ADAMS", "BAKER", "GONZALEZ", "NELSON", "CARTER", "MITCHELL", "PEREZ", "ROBERTS",
"TURNER", "PHILLIPS", "CAMPBELL", "PARKER", "EVANS", "EDWARDS", "COLLINS", "STEWART",
"SANCHEZ", "MORRIS", "ROGERS", "REED", "COOK", "MORGAN", "BELL", "MURPHY", "BAILEY",
"RIVERA", "COOPER", "RICHARDSON", "COX", "HOWARD", "WARD", "TORRES", "PETERSON", "GRAY",
"RAMIREZ", "JAMES", "WATSON", "BROOKS", "KELLY", "SANDERS", "PRICE", "BENNETT", "WOOD",
"BARNES", "ROSS", "HENDERSON", "COLEMAN", "JENKINS", "PERRY", "POWELL", "LONG",
"PATTERSON", "HUGHES", "FLORES", "WASHINGTON", "BUTLER", "SIMMONS", "FOSTER", "GONZALES",
"BRYANT", "ALEXANDER", "RUSSELL", "GRIFFIN", "DIAZ", "HAYES", "MYERS", "FORD", "HAMILTON",
"GRAHAM", "SULLIVAN", "WALLACE", "WOODS", "COLE", "WEST", "JORDAN", "OWENS", "REYNOLDS",
"FISHER", "ELLIS", "HARRISON", "GIBSON", "MCDONALD", "CRUZ", "MARSHALL", "ORTIZ", "GOMEZ",
"MURRAY", "FREEMAN", "WELLS", "WEBB", "SIMPSON", "STEVENS", "TUCKER", "PORTER", "HUNTER",
"HICKS", "CRAWFORD", "HENRY", "BOYD", "MASON", "MORALES", "KENNEDY", "WARREN", "DIXON",
"RAMOS", "REYES", "BURNS", "GORDON", "SHAW", "HOLMES", "RICE", "ROBERTSON", "HUNT",
"BLACK", "DANIELS", "PALMER", "MILLS", "NICHOLS", "GRANT", "KNIGHT", "FERGUSON", "ROSE",
"STONE", "HAWKINS", "DUNN", "PERKINS", "HUDSON", "SPENCER", "GARDNER", "STEPHENS",
"PAYNE", "PIERCE", "BERRY", "MATTHEWS", "ARNOLD", "WAGNER", "WILLIS", "RAY", "WATKINS",
"OLSON", "CARROLL", "DUNCAN", "SNYDER", "HART", "CUNNINGHAM", "BRADLEY", "LANE",
"ANDREWS", "RUIZ", "HARPER", "FOX", "RILEY", "ARMSTRONG", "CARPENTER", "WEAVER", "GREENE",
"LAWRENCE", "ELLIOTT", "CHAVEZ", "SIMS", "AUSTIN", "PETERS", "KELLEY", "FRANKLIN",
"LAWSON", "FIELDS", "GUTIERREZ", "RYAN", "SCHMIDT", "CARR", "VASQUEZ", "CASTILLO",
"WHEELER", "CHAPMAN", "OLIVER", "MONTGOMERY", "RICHARDS", "WILLIAMSON", "JOHNSTON",
"BANKS", "MEYER", "BISHOP", "MCCOY", "HOWELL", "ALVAREZ", "MORRISON", "HANSEN",
"FERNANDEZ", "GARZA", "HARVEY", "LITTLE", "BURTON", "STANLEY", "NGUYEN", "GEORGE",
"JACOBS", "REID", "KIM", "FULLER", "LYNCH", "DEAN", "GILBERT", "GARRETT", "ROMERO",
"WELCH", "LARSON", "FRAZIER", "BURKE", "HANSON", "DAY", "MENDOZA", "MORENO", "BOWMAN",
"MEDINA", "FOWLER", "BREWER", "HOFFMAN", "CARLSON", "SILVA", "PEARSON", "HOLLAND",
"DOUGLAS", "FLEMING", "JENSEN", "VARGAS", "BYRD", "DAVIDSON", "HOPKINS", "MAY", "TERRY",
"HERRERA", "WADE", "SOTO", "WALTERS", "CURTIS", "NEAL", "CALDWELL", "LOWE", "JENNINGS",
"BARNETT", "GRAVES", "JIMENEZ", "HORTON", "SHELTON", "BARRETT", "OBRIEN", "CASTRO",
"SUTTON", "GREGORY", "MCKINNEY", "LUCAS", "MILES", "CRAIG", "RODRIQUEZ", "CHAMBERS",
"HOLT", "LAMBERT", "FLETCHER", "WATTS", "BATES", "HALE", "RHODES")


RECIPES = [ 'asian', 'brown', 'almond', 'beurre', 'blue', 'sausage', 'enchiladas',
'jambalaya', 'blueberry', 'pizza', 'cocktail', 'wine', 'mousse', 'biscuits', 'lait',
'vegetables', 'st.', 'pompano', 'roux', 'waffles', 'cheesecake', 'black', 'pumpkin',
'crabs', 'dipping', 'southeast', 'pot', 'mary', 'mushroom', 'wild', 'squid', 'lemon',
'gras', 'sauce', 'stew', 'chop', 'cranberry', 'curry', 'or', 'fettuccine',
'strawberry', 'fried', 'crepes', 'cafe', 'marinated', 'peanut', "andrea's", 'asparagus',
'seasoning', 'bacon', 'lasagna', 'savory', 'pecans', 'brisket', 'shell', 'hollandaise',
'soft-shell', 'sea', 'creme', 'mussels', 'poivre', 'peppers', 'louisiana', 'quail',
'loin', 'onion', 'old', 'cornbread', 'tenderloin', 'amandine', 'peas',
'tasso', 'apple', 'duck', 'grits', 'mignon', 'rice', 'seared', 'soufflee', 'pancakes',
'cajun', 'jelly', 'orange', 'ham', 'italian', 'bisque', 'turkey', 'snapper',
'filet', 'lobster', 'fresh', 'barbecue', 'broiled', 'hot', 'green', 'redfish', 'potatoes',
'roast', 'eggs', 'scallops', 'eggplant', 'sweet', 'artichokes', 'chili', 'stuffing',
'cream', 'butter', 'spicy', 'dip', 'chocolate', 'lamb', 'artichoke', 'pudding',
'bordelaise', 'salmon', 'rockefeller', 'catfish', 'pepper', 'beans', 'cake', 'dressing',
'crab', 'pecan', 'beef', 'tuna', 'potato', 'fish', 'seafood', 'orleans', 'gratin',
'steak', 'roasted', 'vinaigrette', 'gumbo', 'trout', 'spinach', 'oyster', 'garlic',
'corn', 'bread', 'cheese', 'stuffed', 'bean', 'grilled', 'mushrooms', 'pie', 'pork',
'style', 'red', 'white', 'veal', 'salad', 'crawfish', 'soup', 'creole', 'crabmeat',
'pasta', 'oysters', 'chicken', 'sauce', 'shrimp', ]

PREP = [ 'and', 'nouvelle', 'la', 'new', 'au', 'the', 'de', 'en', 'on', 'or', 'a',
  'for', 'in', 'of', 'with']

PREFIX = ['at', 'chez', 'my', 'le', 'la', 'the', 'bar', 'gourmet', 'on']

DOMAINS = ['gmail.com', 'yahoo.com', 'msn.com', 'facebook.com', 'aol.com', 'att.com']

STREETS = [
  "Second", "Third", "First", "Fourth", "Park", "Fifth", "Main", "Sixth", "Oak", "Seventh",
  "Pine", "Maple", "Cedar", "Eighth", "Elm", "View", "Washington", "Ninth", "Lake", "Hill",
]

STREET_PREFIX = ["North", "South", "East", "West"]
STREET_SUFFIX = ["Boulevard", "Street", "Avenue", "Road", "Way", "Terrace", "Drive",
  "Lane", "Circle", "Place"]

ZIP_CODES = [
"90001",  "90002",  "90003",  "90004",  "90005",  "90006",  "90007",  "90008",  "90009",
"90010",  "90011",  "90012",  "90013",  "90014",  "90015",  "90016",  "90017",  "90018",
"90019",  "90020",  "90021",  "90022",  "90023",  "90024",  "90025",  "90026",  "90027",
"90028",  "90029",  "90030",  "90031",  "90032",  "90033",  "90034",  "90035",  "90036",
"90037",  "90038",  "90039",  "90040",  "90041",  "90042",  "90043",  "90044",  "90045",
"90046",  "90047",  "90048",  "90049",  "90050",  "90051",  "90052",  "90053",  "90054",
"90055",  "90056",  "90057",  "90058",  "90059",  "90060",  "90061",  "90062",  "90063",
"90064",  "90065",  "90066",  "90067",  "90068",  "90070",  "90071",  "90072",  "90073",
"90074",  "90075",  "90076",  "90077",  "90078",  "90079",  "90080",  "90081",  "90082",
"90083",  "90084",  "90086",  "90087",  "90088",  "90089",  "90091",  "90093",  "90094",
"90095",  "90096",  "90097",  "90099",  "90101",  "90102",  "90103",  "90174",  "90185"]


MENUS = [
  ('breakfast', 4, 11),
  ('brunch', 9, 14),
  ('lunch', 11, 18),
  ('dinner', 15, 22),
]

LAT_RANGE = (33.909403, 34.041508)
LON_RANGE = (-118.440857, -118.119507)

ADDRESSES = []

def address(name):
  street = random.choice(STREETS)
  name = "%s at %s" % (name, street)

  # ensure uniqueness
  if name in ADDRESSES:
    return address(name)
  ADDRESSES.append(name)

  return {
    'street': '%s %s %s %s' % (random.randrange(1,2000), random.choice(STREET_PREFIX),
      street, random.choice(STREET_SUFFIX)),
    'zipcode': random.choice(ZIP_CODES),
    'state': 'CA',
    'city': 'Los Angeles',
    'latitude': LAT_RANGE[0] + (random.random() * (LAT_RANGE[1] - LAT_RANGE[0])),
    'longitude': LON_RANGE[0] + (random.random() * (LON_RANGE[1] - LON_RANGE[0])),
  }, name

def restaurant_menu():
  name, start, stop = random.choice(MENUS)
  return {
    'name': name,
    'monday': random.randrange(2),
    'tuesday': random.randrange(2),
    'wednesday': random.randrange(2),
    'thursday': random.randrange(2),
    'friday': random.randrange(2),
    'saturday': random.randrange(2),
    'sunday': random.randrange(2),
    'start': time(start + random.randrange(-1,2), random.randrange(60)),
    'stop': time(stop + random.randrange(-1,2), random.randrange(60)),
  }


user_names = []
def user_name():
  name = "%s %s" % (random.choice(FIRST_NAMES).lower().capitalize(),
    random.choice(LAST_NAMES).lower().capitalize())
  if name not in user_names:
    user_names.append(name)
    return name
  return user_name()

recipe_names = []
def recipe_name():
  nouns = random.sample(RECIPES, random.randrange(2,6))
  nouns.append(random.choice(PREP))
  random.shuffle(nouns)
  name = ' '.join(nouns)
  if name not in recipe_names:
    recipe_names.append(name)
    return name
  return recipe_name()

restaurant_names = []
def restaurant_name():
  name = "%s %s %s" % (
    random.choice(PREFIX).lower().capitalize(),
    random.choice(FIRST_NAMES).lower().capitalize(),
    random.choice(RECIPES).lower().capitalize())
  if name not in restaurant_names:
    restaurant_names.append(name)
    return name
  return restaurant_name()

def user_email(name):
  return "%s@%s" % (name.replace(' ', '.'), random.choice(DOMAINS))

def add_avatar_to_user(user):
  # pick a random avatar for the user
  image_name = '%s.png' % random.randrange(1,31)
  image_path = 'app/bin/avatar/%s' % image_name
  image = Image()
  image.size = 300
  image.owner = user
  image.album = 'dishes'
  image.file = photos.save(FileStorage(stream=open(image_path), filename=image_name))
  db.session.add(image)
  db.session.commit()
  user.image_id = image.id

users = []

def paragraph(min=1, max=20):
  def sentence():
    nouns = ["time",  "person",  "year",  "way",  "day",  "thing",  "man",  "world",
    "life",  "hand", "part",  "child",  "eye",  "woman",  "place",  "work",  "week",
    "case",  "point", "government",  "company",  "number",  "group",  "problem", "fact"]
    verbs = ["be",  "have",  "do",  "say",  "get",  "make",  "go",  "know",  "take",
    "see",  "come", "think",  "look",  "want",  "give",  "use",  "find",  "tell",  "ask",
    "work",  "seem", "feel",  "try",  "leave",  "call"]
    adjectives = ["good",  "new",  "first",  "last",  "long",  "great",  "little",
      "own", "other",  "old", "right",  "big",  "high",  "different",  "small",
      "large",  "next",  "early",  "young", "important",  "few",  "public",
      "bad",  "same",  "able"]
    prepositions = ["to",  "of",  "in",  "for",  "on",  "with",  "at",  "by",  "from",
      "up",  "about", "into",  "over",  "after",  "beneath",  "under",  "above"]
    adverbs = ["and", "therefore", "then", "but", "while", "so", "in order to",
      "thereafter", "also", "besides", "consequently", "finally", "hence", "in addition",
      "However", "furthermore", "meanwhile", "likewise", "instead", "indeed"]

    def maker():
      words = []
      if random.choice([1,0,0]): # 1 out of 3
        words.append(random.choice(prepositions))
      if random.choice([1,0,0]): # 1 out of 3
        words.append(random.choice(adjectives))
      words.append(random.choice(nouns))
      words.append(random.choice(verbs))
      if random.choice([1,0,0]): # 1 out of 3
        words.append(random.choice(prepositions))
      if random.choice([1,1,0]): # 2 out of 3
        words.append(random.choice(adjectives))
      words.append(random.choice(nouns))
      return words

    _words = maker()
    if random.choice([1,0,0]): # 1 out of 3
      _words.append(random.choice(adverbs))
      _words += maker()
    return ' '.join(_words).capitalize()

  sentences = []
  for x in range(random.randrange(min,max)):
    sentences.append(sentence())
  return '. '.join(sentences) + random.choice('??!...')

def run():
  USERS = 200
  RECIPES = 200
  RESTAURANTS = 50
  DISH_RANGE = [10,40]
  DISH_PRICE_RANGE = [5, 80]
  PLACE_RANGE = [1, 3]

  print "creating users (%s)..." % USERS
  for x in range(USERS):
    name = user_name()
    email = user_email(name)
    user = _createUser(name, email, 'password')
    add_avatar_to_user(user)
    db.session.commit()
    users.append(user)
  print "done"

  print "creating recipes (%s)..." % RECIPES
  for x in range(RECIPES):
    recipe = Recipe(name=recipe_name())
    recipe.category_id = random.choice(CATEGORIES)[0]
    recipe.ingredients = random.sample(INGREDIENTS, random.randrange(2,8))
    db.session.add(recipe)
    db.session.commit()
  print "done"

  RECIPES = Recipe.query.all()

  restaurants = []
  print "creating restaurants (%s)..." % RESTAURANTS
  for x in range(RESTAURANTS):
    name = user_name()
    email = user_email(name)
    user = _createUser(name, email, 'password')
    add_avatar_to_user(user)
    restaurant = _create_restaurant(restaurant_name(), user=user)
    users.append(user)
    restaurants.append(restaurant)
    db.session.commit()

    # create place
    for x in range(random.randrange(*PLACE_RANGE)):
      place_address, place_name = address(restaurant.name)
      place = Place(**place_address)
      place.name = place_name
      place.restaurant = restaurant
      db.session.add(place)
    db.session.commit()

    # create dishes
    for recipe in random.sample(RECIPES, random.randrange(*DISH_RANGE)):
      # pick a random image for the dish
      image_name = '%s.jpeg' % random.randrange(1,21)
      image_path = 'app/bin/images/%s' % image_name
      image = Image()
      image.size = 600
      image.owner = user
      image.album = 'dishes'
      image.file = photos.save(FileStorage(stream=open(image_path), filename=image_name))
      db.session.add(image)

      dish = Dish(name=recipe.name)
      dish.ingredients = recipe.ingredients
      dish.category_id = recipe.category_id
      dish.ingredients.append(random.choice(INGREDIENTS))
      dish.hero_ingredient = random.choice(dish.ingredients)
      dish.restaurant = restaurant
      dish.recipe = recipe
      dish.image = image
      db.session.add(dish)
    db.session.commit()

    # create menu associate it with every places and put all the dishes in it
    menu = Menu(**restaurant_menu())
    menu.restaurant = restaurant
    menu.places = restaurant.places.all()
    db.session.add(menu)
    for dish in restaurant.dishes:

      menu_dish = MenuDish()
      menu_dish.menu = menu
      menu_dish.name = dish.name
      menu_dish.dishes = [dish]
      menu_dish.price = DISH_PRICE_RANGE[0] + (int(random.random() * 100 * (DISH_PRICE_RANGE[1] - DISH_PRICE_RANGE[0]))/100.0)
      db.session.add(menu_dish)
    db.session.commit()
  print "done"

  # lets add some post and comments
  print "creating posts (%s)..." % (len(users) * 3)
  posts = []
  for user in users * 3:
    post = RestaurantPost()
    post.restaurant = random.choice(restaurants)
    post.author = user
    post.content = paragraph()
    if random.choice([0,0,1]):
      # pick a random image for the post
      image_name = '%s.jpeg' % random.randrange(1,21)
      image_path = 'app/bin/images/%s' % image_name
      image = Image()
      image.size = 600
      image.owner = user
      image.album = 'posts'
      image.file = photos.save(FileStorage(stream=open(image_path), filename=image_name))
      db.session.add(image)
      post.image = image

    db.session.add(post)
    posts.append(post)
    db.session.commit()
  print "done"

  print "creating comments to posts..."
  for x in range(random.randrange(len(posts), len(posts) * 3)):
    comment = RestaurantPostComment()
    comment.post = random.choice(posts)
    comment.author = random.choice(users)
    comment.content = paragraph()
    db.session.add(comment)
    db.session.commit()
  print "done"

