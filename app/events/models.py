from datetime import datetime
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy import event

from app import db

class EventAssociation(db.Model):
  __tablename__ = "events_association"

  id = db.Column(db.Integer, primary_key=True)
  discriminator = db.Column(db.String)

  @classmethod
  def creator(cls, discriminator):
    return lambda events: EventAssociation(
                          events = events,
                          discriminator=discriminator)

  @property
  def parent(self):
    return getattr(self, "%s_parent" % self.discriminator)


class Event(db.Model):
  __tablename__ = "events_event"

  id = db.Column(db.Integer, primary_key=True)
  author_id = db.Column(db.Integer, db.ForeignKey("identities_user.id"))
  author = db.relationship("User",
             backref=db.backref("events", lazy="dynamic"))
  creation_date = db.Column(db.DateTime, default=datetime.utcnow)

  association_id = db.Column(db.Integer, db.ForeignKey("events_association.id"))
  association = db.relationship("EventAssociation", backref="events")
  parent = association_proxy("association", "parent")


class HasEvent(object):
  @declared_attr
  def event_association_id(cls):
     return db.Column(db.Integer, db.ForeignKey("events_association.id"))

  @declared_attr
  def event_association(cls):
    discriminator = cls.__name__.lower()
    cls.events = association_proxy(
                  "event_association", "events",
                  creator=EventAssociation.creator(discriminator)
              )
    return db.relationship("EventAssociation",
              backref=db.backref("%s_parent" % discriminator, uselist=False))

  @classmethod
  def __declare_last__(cls):
    event.listen(cls, "before_insert", cls.before_insert_parent)
    event.listen(cls, "before_delete", cls.before_delete_parent)

  @staticmethod
  def before_insert_parent(mapper, connection, target):
    target.events = [Event(author=target.author)]

  @staticmethod
  def before_delete_parent(mapper, connection, target):
    db.session.delete(target.events )

