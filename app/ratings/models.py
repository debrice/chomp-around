from app import db

class Rating(object):
  pass

class DishRating(Rating, db.Model):
  __tablename__ = 'ratings_dishrating'
  __table_args__ = (
    db.UniqueConstraint("place_id", "restaurant_id"),
  )

  id = db.Column(db.Integer, primary_key=True)
  bonus = db.Column(db.Integer)
  minus = db.Column(db.Integer)

  place_id = db.Column(db.Integer)
  place = db.relationship('Place', backref=db.backref('ratings', lazy='dynamic'))
  dish_id = db.Column(db.Integer)
  dish = db.relationship('Dish', backref=db.backref('ratings', lazy='dynamic'))


class DishUserRating(db.Model):
  __tablename__ = 'ratings_dishrating'
  __table_args__ = (
    db.UniqueConstraint("user_id", "dishrating_id"),
  )

  id = db.Column(db.Integer, primary_key=True)
  value = db.Column(db.Integer)

  user_id = db.Column(db.Integer)
  user = db.relationship('User', backref=db.backref('dish_ratings', lazy='dynamic'))

  dishrating_id = db.Column(db.Integer)
  dishrating = db.relationship('DishRating', backref=db.backref('users', lazy='dynamic'))

