from app import db

from .constants import CATEGORIES, VALID, STATUS, ENTREE


# ManyToMany tables
recipes_ingredients = db.Table('recipes_ingredients',
  db.Column('ingredient_id', db.Integer, db.ForeignKey('recipes_ingredient.id')),
  db.Column('recipe_id', db.Integer, db.ForeignKey('recipes_recipe.id'))
)

recipe_tags = db.Table('recipes_recipe_tags',
  db.Column('tag_id', db.Integer, db.ForeignKey('recipes_tag.id')),
  db.Column('recipe_id', db.Integer, db.ForeignKey('recipes_recipe.id'))
)

ingredient_categories = db.Table('recipes_ingredient_categories',
  db.Column('category_id', db.Integer, db.ForeignKey('recipes_ingredient_category.id')),
  db.Column('ingredient_id', db.Integer, db.ForeignKey('recipes_ingredient.id'))
)

ingredients_ingredients = db.Table('recipes_ingredients_ingredients',
  db.Column('parent_ingredient_id', db.Integer,
    db.ForeignKey('recipes_ingredient.id'), primary_key=True),
  db.Column('child_ingredient_id', db.Integer,
    db.ForeignKey('recipes_ingredient.id'), primary_key=True)
)


class Tag(db.Model):
  """
  Tag associated with recipes
  """
  __tablename__ = 'recipes_tag'

  name = db.Column(db.String(255))
  id = db.Column(db.Integer, primary_key=True)


class Recipe(db.Model):
  __tablename__ = 'recipes_recipe'

  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(255), unique=True)
  category_id = db.Column(db.Integer, default=ENTREE)
  status_id = db.Column(db.Integer, default=VALID)

  tags = db.relationship('Tag', secondary=recipe_tags,
      backref=db.backref('recipes', lazy='dynamic'))
  ingredients = db.relationship('Ingredient', secondary=recipes_ingredients,
      backref=db.backref('recipes', lazy='dynamic'))

  @property
  def category(self):
    return dict(CATEGORIES)[self.category_id]

  @property
  def status(self):
    return dict(STATUS)[self.status_id]

  def __repr__(self):
    return u"<Recipe:%d %s>" % (self.id, self.name)


class Ingredient(db.Model):
  __tablename__ = 'recipes_ingredient'

  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(255), unique=True)
  status_id = db.Column(db.Integer, default=VALID)

  parents = db.relationship('Ingredient', secondary=ingredients_ingredients,
          primaryjoin=id==ingredients_ingredients.c.child_ingredient_id,
          secondaryjoin=id==ingredients_ingredients.c.parent_ingredient_id,
          backref=db.backref('children', lazy='dynamic'))
  categories = db.relationship('IngredientCategory', secondary=ingredient_categories,
          backref=db.backref('ingredients', lazy='dynamic'))

  def __contains__(self, item):
    if item.id == self.id:
      return True
    return any(item in parent for parent in self.parents)

  @property
  def status(self):
    return dict(STATUS)[self.status_id]

  def __repr__(self):
    return u"<Ingredient:%d %s>" % (self.id, self.name)


class IngredientCategory(db.Model):
  __tablename__ = 'recipes_ingredient_category'

  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(255), unique=True)

  def __repr__(self):
    return u"<IngredientCategory:%d %s>" % (self.id, self.name)


# query factories
def get_ingredients():
  return Ingredient.query.filter()


def get_categories():
  return IngredientCategory.query.filter()


def get_tags():
  return Tag.query.filter()


def get_recipes():
  return Recipe.query.filter()
