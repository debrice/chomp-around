SUGGESTION = 2
VALID = 1

APPETIZER = 1
SALAD = 2
ENTREE = 3
SIDE = 4
DESSERT = 5
DRINK = 6
SANDWICH = 7

STATUS = (
  (SUGGESTION, 'suggestion'),
  (VALID, 'valid'),
)

CATEGORIES = (
  (APPETIZER, "appetizer"),
  (SALAD, "salad"),
  (ENTREE, "entree"),
  (SIDE, "side"),
  (DESSERT, "dessert"),
  (DRINK, "drink"),
  (SANDWICH, "sandwich"),
)
