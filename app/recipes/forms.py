from flask.ext.wtf import Form, TextField, Required, QuerySelectMultipleField, \
  Length, ValidationError, SelectField, QuerySelectField
from app.lib.widgets import auto_complete
from app.lib.fields import ListTextField

from .models import IngredientCategory, Recipe, Ingredient, get_ingredients, \
  Tag, get_categories, get_recipes, get_tags
from .constants import CATEGORIES, STATUS

class IngredientForm(Form):
  name = TextField("Ingredient's name", [Required(), Length(min=3, max=255)])

  def __init__(self, *args, **kwargs):
    super(IngredientForm, self).__init__(*args, **kwargs)
    self.ingredient = kwargs.get('obj')

  def validate_name(self, field):
    if self.ingredient and self.ingredient.name == field.data:
      return

    if Ingredient.query.filter_by(name=field.data).all():
      raise ValidationError('Ingredient "%s" already exist.' % field.data)


class SubstituteIngredientForm(Form):
  ingredient = QuerySelectField(u'Ingredient', get_label='name', allow_blank=False,
    query_factory=get_ingredients, widget=auto_complete)


class FullIngredientForm(IngredientForm):
  parents = QuerySelectMultipleField(u'Parent', get_label='name', allow_blank=True,
    query_factory=get_ingredients, widget=auto_complete)
  categories = QuerySelectMultipleField(u'Category', get_label='name', allow_blank=True,
    query_factory=get_categories, widget=auto_complete)
  status_id = SelectField('Status', [Required()], choices=STATUS, coerce=int)


class SubstituteRecipeForm(Form):
  recipe = QuerySelectField(u'Recipe', get_label='name', allow_blank=False,
    query_factory=get_recipes, widget=auto_complete)


class RecipeForm(Form):
  name = TextField("Recipe's name", [Required(), Length(min=3, max=255)])

  def __init__(self, *args, **kwargs):
    super(RecipeForm, self).__init__(*args, **kwargs)
    self.recipe = kwargs.get('obj')

  def validate_name(self, field):
    if self.recipe and self.recipe.name == field.data:
      return

    if Recipe.query.filter_by(name=field.data).all():
      raise ValidationError('Recipe "%s" already exist.' % field.data)


class FullRecipeForm(RecipeForm):
  category_id = SelectField(choices=CATEGORIES, coerce=int)
  ingredients_suggest = ListTextField()
  status_id = SelectField('Status', choices=STATUS, coerce=int)
  tags = QuerySelectMultipleField('Tag', get_label='name', allow_blank=True,
    query_factory=get_tags, widget=auto_complete)
  ingredients = QuerySelectMultipleField(u'Ingredients', get_label='name', allow_blank=True,
    query_factory=get_ingredients, widget=auto_complete)


class TagForm(Form):
  name = TextField("Tag's name", [Required(), Length(min=3, max=255)])

  def __init__(self, *args, **kwargs):
    super(TagForm, self).__init__(*args, **kwargs)
    self.tag = kwargs.get('obj')

  def validate_name(self, field):
    if self.tag and self.tag.name == field.data:
      return

    if Tag.query.filter_by(name=field.data).all():
      raise ValidationError('Tag "%s" already exist.' % field.data)


class IngredientCategoryForm(Form):
  name = TextField("Category's name", [Required(), Length(min=3, max=255)])

  def __init__(self, *args, **kwargs):
    super(IngredientCategoryForm, self).__init__(*args, **kwargs)
    self.category = kwargs.get('obj')

  def validate_name(self, field):
    if self.category and self.category.name == field.data:
      return

    if IngredientCategory.query.filter_by(name=field.data).all():
      raise ValidationError('Category "%s" already exist.' % field.data)

class FilterForm(Form):
  name = TextField("Filter by name")
