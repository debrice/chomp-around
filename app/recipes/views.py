from flask import Blueprint, render_template, flash, redirect, url_for, \
  request, Markup
from flask.ext.login import fresh_login_required

from app import db
from app.lib.decorators import remote

from app.identities.decorators import admin_required
from .models import Recipe, Ingredient, IngredientCategory, Tag
from .constants import SUGGESTION, VALID
from .forms import IngredientCategoryForm, IngredientForm, FullIngredientForm,\
  RecipeForm, FullRecipeForm, SubstituteIngredientForm, FilterForm, \
  SubstituteRecipeForm, TagForm

mod = Blueprint('recipes', __name__, url_prefix='/recipes')


@mod.route('/ingredient/edit/<int:id>/', methods=['GET', 'POST'])
@mod.route('/ingredient/edit/<int:id>/<int:page>/', methods=['GET', 'POST'])
@fresh_login_required
@admin_required
def edit_ingredient(id, page=1):
  ingredient = Ingredient.query.get_or_404(id)

  form = FullIngredientForm(obj=ingredient)
  if form.validate_on_submit():
    ingredient.name = form.name.data
    # prevent infinite recursive relation like x -> y -> z -> x -> y -> z...
    # by removing parent being child of the current ingredient
    ingredient.parents = [p for p in form.parents.data if ingredient not in p]
    ingredient.categories = form.categories.data
    ingredient.status_id = form.status_id.data
    db.session.commit()
    flash('Ingredient successfully updated', 'success')
    return redirect(url_for('recipes.ingredients'))

  kwargs = {
    "form": form,
    "ingredient": ingredient,
    "recipes": ingredient.recipes.order_by('name'),
    "ingredients": ingredient.children.order_by('name').paginate(page),
  }

  if ingredient.status_id == SUGGESTION:
    kwargs['substitute_form'] = SubstituteIngredientForm()

  return render_template("recipes/edit_ingredient.html", **kwargs)


@mod.route('/tag/<int:id>/delete/', methods=['POST'])
@fresh_login_required
@admin_required
def delete_tag(id):
  tag = Tag.query.get_or_404(id)
  db.session.delete(tag)
  db.session.commit()
  flash("Tag %s deleted." % tag.name, 'success')
  return redirect(url_for('recipes.tags'))


@mod.route('/ingredient/<int:id>/delete/', methods=['POST'])
@fresh_login_required
@admin_required
def delete_ingredient(id):
  ingredient = Ingredient.query.get_or_404(id)
  db.session.delete(ingredient)
  db.session.commit()
  flash("Ingredient %s deleted." % ingredient.name, 'success')
  return redirect(url_for('recipes.ingredients'))


@mod.route('/recipe/<int:id>/substitute/',  methods=['POST'])
@fresh_login_required
@admin_required
def recipe_substitute(id):
  recipe = Recipe.query.get_or_404(id)
  form = SubstituteRecipeForm()
  if form.validate_on_submit():
    substitute = form.recipe.data
    for dish in recipe.dishes:
      if dish.recipe_id != substitute.id:
        dish.recipe = substitute
    db.session.delete(recipe)
    db.session.commit()
    flash(
      Markup('Recipe <strong>%s</strong> substituted by <strong>%s</strong>')\
      % (recipe.name, substitute.name), 'success')
    return redirect(url_for('recipes.edit_recipe', id=substitute.id))
  return redirect(url_for('recipes.edit_recipe', id=recipe.id))


@mod.route('/ingredient/<int:id>/substitute/', methods=['POST'])
@fresh_login_required
@admin_required
def ingredient_substitute(id):
  ingredient = Ingredient.query.get_or_404(id)
  form = SubstituteIngredientForm()
  if form.validate_on_submit():
    substitute = form.ingredient.data
    for recipe in ingredient.recipes:
      if substitute not in recipe.ingredients:
        recipe.ingredients.append(substitute)
    for dish in ingredient.dishes:
      if substitute not in dish.ingredients:
        dish.ingredients.append(substitute)
    db.session.delete(ingredient)
    db.session.commit()
    flash(
      Markup('Ingredient <strong>%s</strong> substituted by <strong>%s</strong>')\
      % (ingredient.name, substitute.name), 'success')
    return redirect(url_for('recipes.edit_ingredient', id=substitute.id))
  return redirect(url_for('recipes.edit_ingredient', id=ingredient.id))


@mod.route('/ingredient/suggestions/', methods=['GET', 'POST'])
@mod.route('/ingredient/suggestions/<int:page>/', methods=['GET', 'POST'])
@fresh_login_required
@admin_required
def ingredient_suggestions(page=1):
  return render_template("recipes/ingredient_suggestions.html",
    ingredients=Ingredient.query.filter_by(status_id=SUGGESTION).\
      order_by('name').paginate(page))


@mod.route('/ingredients/', methods=['GET', 'POST'])
@mod.route('/ingredients/<int:page>/', methods=['GET', 'POST'])
@fresh_login_required
@admin_required
def ingredients(page=1):
  form = IngredientForm()
  if form.validate_on_submit():
    ingredient = Ingredient()
    ingredient.name = form.name.data
    db.session.add(ingredient)
    db.session.commit()
    flash('Ingredient successfully created', 'success')
    return redirect(url_for('recipes.edit_ingredient', id=ingredient.id))

  args = [Ingredient.status_id == VALID]

  filter_form = FilterForm(request.args, prefix='filter', csrf_enabled=False)
  if filter_form.validate() and filter_form.name.data:
    args.append(Ingredient.name.like('%%%s%%' % filter_form.name.data))

  return render_template("recipes/ingredients.html", form=form,
    filter_form=filter_form,
    ingredients=Ingredient.query.filter(*args).order_by('name').paginate(page))


@mod.route('/category/edit/<int:id>/', methods=['GET', 'POST'])
@mod.route('/category/edit/<int:id>/<int:page>/', methods=['GET', 'POST'])
@fresh_login_required
@admin_required
def edit_category(id, page=1):
  category = IngredientCategory.query.get_or_404(id)
  form = IngredientCategoryForm(obj=category)
  if form.validate_on_submit():
    category.name = form.name.data
    db.session.commit()
    flash('Category successfully updated', 'success')
    return redirect(url_for('recipes.categories'))

  return render_template("recipes/edit_category.html", form=form,
    ingredients=category.ingredients.order_by('name').paginate(page),
    category=category)


@mod.route('/categories/', methods=['GET', 'POST'])
@mod.route('/categories/<int:page>/', methods=['GET', 'POST'])
@fresh_login_required
@admin_required
def categories(page=1):
  form = IngredientCategoryForm()
  if form.validate_on_submit():
    category = IngredientCategory()
    category.name = form.name.data
    db.session.add(category)
    db.session.commit()
    flash('Category successfully created', 'success')
    return redirect(url_for('recipes.categories'))

  args = []
  filter_form = FilterForm(request.args, prefix='filter', csrf_enabled=False)
  if filter_form.validate() and filter_form.name.data:
    args.append(IngredientCategory.name.like('%%%s%%' % filter_form.name.data))

  return render_template("recipes/categories.html", form=form,
    categories=IngredientCategory.query.filter(*args).\
      order_by('name').paginate(page), filter_form=filter_form)


@mod.route('/tag/edit/<int:id>/', methods=['GET', 'POST'])
@mod.route('/tag/edit/<int:id>/<int:page>/', methods=['GET', 'POST'])
@fresh_login_required
@admin_required
def edit_tag(id, page=1):
  tag = Tag.query.get_or_404(id)
  form = TagForm(obj=tag)
  if form.validate_on_submit():
    tag.name = form.name.data
    db.session.commit()
    flash('Tag successfully updated', 'success')
    return redirect(url_for('recipes.tags'))

  return render_template("recipes/edit_tag.html", form=form,
    recipes=tag.recipes.order_by('name').paginate(page),
    tag=tag)


@mod.route('/tags/', methods=['GET', 'POST'])
@mod.route('/tags/<int:page>/', methods=['GET', 'POST'])
@fresh_login_required
@admin_required
def tags(page=1):
  form = TagForm()
  if form.validate_on_submit():
    tag = Tag(name=form.name.data)
    db.session.add(tag)
    db.session.commit()
    flash('Tag successfully created', 'success')
    return redirect(url_for('recipes.tags'))

  args = []
  filter_form = FilterForm(request.args, prefix='filter', csrf_enabled=False)
  if filter_form.validate() and filter_form.name.data:
    args.append(Tag.name.like('%%%s%%' % filter_form.name.data))

  return render_template("recipes/tags.html", form=form,
    tags=Tag.query.filter(*args).order_by('name').paginate(page),
    filter_form=filter_form)


@mod.route('/recipes/suggestions/', methods=['GET', 'POST'])
@mod.route('/recipes/suggestions/<int:page>/', methods=['GET', 'POST'])
@fresh_login_required
@admin_required
def recipe_suggestions(page=1):
  return render_template("recipes/recipe_suggestions.html",
    recipes=Recipe.query.filter_by(status_id=SUGGESTION).\
      order_by('name').paginate(page))


@mod.route('/recipes/', methods=['GET', 'POST'])
@mod.route('/recipes/<int:page>/', methods=['GET', 'POST'])
@fresh_login_required
@admin_required
def recipes(page=1):
  form = RecipeForm()
  if form.validate_on_submit():
    recipe = Recipe()
    recipe.name = form.name.data
    db.session.add(recipe)
    db.session.commit()
    flash('Recipe successfully created', 'success')
    return redirect(url_for('recipes.edit_recipe', id=recipe.id))

  args = [Recipe.status_id == 1]
  filter_form = FilterForm(request.args, prefix='filter', csrf_enabled=False)
  if filter_form.validate() and filter_form.name.data:
    args.append(Recipe.name.like('%%%s%%' % filter_form.name.data))

  return render_template("recipes/recipes.html", form=form,
    filter_form=filter_form,
    recipes=Recipe.query.filter(*args).order_by('name').paginate(page))


def suggested_ingredient(name):
  """
  get or create the ingredient
  """
  name = name.lower().strip()
  ingredient_form = IngredientForm(csrf_enabled=False, name=name)
  if ingredient_form.validate():
    # first try to find the ingredient
    ingredient = Ingredient.query.filter_by(name=name).first()
    if ingredient:
      return ingredient

    ingredient = Ingredient(name=name)
    ingredient.status_id = SUGGESTION
    return ingredient


def suggested_recipe(name, **kwargs):
  name = name.lower().strip()
  recipe_form = FullRecipeForm(csrf_enabled=False, name=name,
    status_id=SUGGESTION, **kwargs)
  if recipe_form.validate():
    recipe = Recipe.query.filter_by(name=name).first()
    if recipe:
      return recipe
    recipe = Recipe(name=name)
    recipe.status_id = recipe_form.status_id.data
    recipe.category_id = recipe_form.category_id.data
    recipe.ingredients = recipe_form.ingredients.data
    return recipe


@mod.route('/recipe/edit/<int:id>/', methods=['GET', 'POST'])
@mod.route('/recipe/edit/<int:id>/<int:page>/', methods=['GET', 'POST'])
@fresh_login_required
@admin_required
def edit_recipe(id, page=1):
  recipe = Recipe.query.get_or_404(id)

  form = FullRecipeForm(obj=recipe)
  if form.validate_on_submit():
    recipe.name = form.name.data
    recipe.category_id = form.category_id.data
    recipe.ingredients = form.ingredients.data
    recipe.tags = form.tags.data

    for ingredient in form.ingredients_suggest.data:
      ingredient = suggested_ingredient(ingredient)
      if ingredient:
        recipe.ingredients.append(ingredient)

    db.session.commit()
    flash('Recipe successfully updated', 'success')
    return redirect(url_for('recipes.recipes'))

  kwargs = {
    "form": form,
    "recipe": recipe,
    "dishes": recipe.dishes.order_by('name').paginate(page),
  }

  if recipe.status_id == SUGGESTION:
    kwargs['substitute_form'] = SubstituteRecipeForm()

  return render_template("recipes/edit_recipe.html", **kwargs)


@mod.route('/r/ingredients/', methods=['GET'])
@remote
def remote_ingredients():
  args = []
  if 'ids' in request.values:
    args.append(Ingredient.id.in_(request.values['ids'].split(',')))
  if 'q' in request.values:
    args.append(Ingredient.name.like('%%%s%%' % request.values['q']))
  if 'x' in request.values:
    args.append(~Ingredient.id.in_(request.values['x'].split(',')))
  if 'all' not in request.values:
    args.append(Ingredient.status_id == VALID)

  data = Ingredient.query.filter(*args).limit(20)
  return dict((x.id, x.name) for x in data)


@mod.route('/r/tags/', methods=['GET'])
@remote
def remote_tags():
  args = []
  if 'ids' in request.values:
    args.append(Tag.id.in_(request.values['ids'].split(',')))
  if 'q' in request.values:
    args.append(Tag.name.like('%%%s%%' % request.values['q']))
  if 'x' in request.values:
    args.append(~Tag.id.in_(request.values['x'].split(',')))

  data = Tag.query.filter(*args).limit(20)
  return dict((x.id, x.name) for x in data)


@mod.route('/r/categories/', methods=['GET'])
@remote
def remote_categories():
  args = []
  if 'ids' in request.values:
    args.append(IngredientCategory.id.in_(request.values['ids'].split(',')))
  if 'q' in request.values:
    args.append(IngredientCategory.name.like('%%%s%%' % request.values['q']))
  if 'x' in request.values:
    args.append(~IngredientCategory.id.in_(request.values['x'].split(',')))

  data = IngredientCategory.query.filter(*args).limit(20)
  return dict((x.id, x.name) for x in data)


@mod.route('/r/recipes/', methods=['GET'])
@remote
def remote_recipes():
  args = []
  if 'ids' in request.values:
    args.append(Recipe.id.in_(request.values['ids'].split(',')))
  if 'q' in request.values:
    args.append(Recipe.name.like('%%%s%%' % request.values['q']))
  if 'x' in request.values:
    args.append(~Recipe.id.in_(request.values['x'].split(',')))

  data = Recipe.query.filter(*args).limit(20)
  return dict((x.id, x.name) for x in data)


@mod.route('/r/recipe/', methods=['GET'])
@remote
def remote_recipe():
  """
  return ingredients contained in a recipe.
  """
  id = request.args.get('id')
  recipe = Recipe.query.get_or_404(id)
  return {
    "ingredients": dict((x.id, x.name) for x in recipe.ingredients),
    "category": recipe.category,
  }
