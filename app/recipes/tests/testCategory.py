from urllib import urlencode

from flask import url_for

from app.lib.testing import TestCase
from app.identities.tests import fixtures as identity_fixtures

from .fixtures import all_data, env
from ..models import IngredientCategory

env.update(identity_fixtures.env)


class TestCategory(TestCase):
  env = env
  datasets = all_data + identity_fixtures.all_data

  create_data = {
    'name': 'cereal',
  }

  edit_data = {
    'name': 'spice',
  }

  def test_non_admin_access(self):
    """
    Ensure regular users can't edit or create categories
    """
    login_URL = url_for('identities.login')

    create_URL = url_for('recipes.categories')
    response = self.login(ID=self.data.IdentityData.ActiveUser)
    response = self.client.get(create_URL)
    self.assertRedirects(response,
                         '%s?%s' % (login_URL, urlencode({"next": create_URL})))

    edit_URL = url_for('recipes.edit_category',
                       id=self.data.IngredientCategoryData.Dairy.id)
    response = self.login(ID=self.data.IdentityData.ActiveUser)
    response = self.client.get(edit_URL)
    self.assertRedirects(response,
                         '%s?%s' % (login_URL, urlencode({'next':edit_URL})))

  def test_create_category(self):
    """
    Ensure regular users can't edit or create categories
    """
    response = self.login(ID=self.data.IdentityData.Admin)

    response = self.client.get(url_for('recipes.categories'))
    self.assertStatus(response, 200)

    response = self.client.post(url_for('recipes.categories'),
      data=self.create_data)

    # ensure category got created
    self.assertExist(IngredientCategory, **self.create_data)

    self.assertRedirects(response, url_for('recipes.categories'))

  def test_create_unique_name(self):
    """
    Ensure can't create 2 categories with the same name
    """
    create_data = {"name": self.data.IngredientCategoryData.Cheese.name}
    initial = IngredientCategory.query.filter_by(**create_data).count()

    response = self.login(ID=self.data.IdentityData.Admin)

    response = self.client.post(url_for('recipes.categories'), data=create_data)
    self.assertStatus(response, 200)
    self.assertIn("already exist", response.data)

    # ensure no record got created
    self.assertEqual(IngredientCategory.query.filter_by(**create_data).count(),
      initial)

  def test_edit_category(self):
    from_category = self.data.IngredientCategoryData.Cheese
    to_category = self.data.IngredientCategoryData.Dairy

    edit_url = url_for('recipes.edit_category', id=from_category.id)

    response = self.login(ID=self.data.IdentityData.Admin)

    # ensure uniqueness
    response = self.client.post(edit_url,
                     data={"name": to_category.name})
    self.assertIn("already exist", response.data)
    self.assertNotEqual(IngredientCategory.query.get(from_category.id).name,
      self.data.IngredientCategoryData.Dairy.name)

    # ensure record got updated
    response = self.client.post(edit_url, data=self.edit_data)
    self.assertRedirects(response, url_for('recipes.categories'))
    self.assertNotIn("already exist", response.data)

    category = IngredientCategory.query.filter_by(**self.edit_data).first()
    self.assertTrue(category)
    self.assertEqual(category.id, self.data.IngredientCategoryData.Cheese.id)
    self.assertEqual(category.name, self.edit_data['name'])
