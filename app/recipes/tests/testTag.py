from flask import url_for

from app.lib.testing import TestCase
from app.identities.tests import fixtures as identity_fixtures

from .fixtures import all_data, env
from ..models import Tag, Recipe

env.update(identity_fixtures.env)

class TestTag(TestCase):
  env = env
  datasets = all_data + identity_fixtures.all_data


  def test_delete_tag(self):
    """
    Deleting tag removes it from every recipe
    """
    tag = self.data.TagData.Pizza
    delete_URL = url_for('recipes.delete_tag', id=tag.id)
    # ensure only admin can access delete tag feature
    self.login(ID=self.data.IdentityData.ActiveUser)
    rv = self.client.post(delete_URL)
    self.assertStatus(rv, 302)
    self.assertPath(rv, url_for('identities.login'))

    self.login(ID=self.data.IdentityData.Admin)

    # ensure GET doesn't delete the object
    rv = self.client.get(delete_URL)
    self.assertStatus(rv, 405)
    self.assertIsNotNone(Tag.query.get(tag.id))

    # delete the tag and ensure it got deleted from recipes
    recipes = tag.recipes
    # lets make sure at least one recipe is associated with this tag
    # since tags property is lazy, no db called occured to retriev tags yet.
    self.assertTrue(len(recipes))

    rv = self.client.post(delete_URL)
    self.assertRedirects(rv, url_for('recipes.tags'))
    self.assertIsNone(Tag.query.get(tag.id))

    # now we are calling tags, retrieving data from DB
    for recipe in recipes:
      self.assertNotIn(tag.id, [i.id for i in recipe.tags])
