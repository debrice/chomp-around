from urllib import urlencode

from flask import url_for

from app.lib.testing import TestCase
from app.identities.tests import fixtures as identity_fixtures
from app.restaurants.tests import fixtures as restaurant_fixtures
from app.restaurants.models import Dish

from .fixtures import all_data, env
from ..models import Ingredient, Recipe

env.update(identity_fixtures.env)
env.update(restaurant_fixtures.env)


class TestIngredient(TestCase):
  env = env
  datasets = all_data + identity_fixtures.all_data + \
    restaurant_fixtures.all_data

  create_data = {
    'name': 'garlic',
  }

  edit_data = {
    'name': 'Greek Yogourt',
  }

  def test_delete_ingredient(self):
    """
    Deleting ingredient removes ingredient from recipes and dishes
    """
    ingredient = self.data.IngredientData.Mozarella
    delete_URL = url_for('recipes.delete_ingredient', id=ingredient.id)
    # ensure only admin can access delete ingredient feature
    self.login(ID=self.data.IdentityData.ActiveUser)
    rv = self.client.post(delete_URL)
    self.assertStatus(rv, 302)
    self.assertPath(rv, url_for('identities.login'))

    self.login(ID=self.data.IdentityData.Admin)

    # ensure GET doesn't delete the object
    rv = self.client.get(delete_URL)
    self.assertStatus(rv, 405)
    self.assertIsNotNone(Ingredient.query.get(ingredient.id))

    # delete the ingredient and ensure it got deleted from dishes and recipes
    rv = self.client.post(delete_URL)
    self.assertRedirects(rv, url_for('recipes.ingredients'))
    self.assertIsNone(Ingredient.query.get(ingredient.id))
    dish = Dish.query.get(self.data.DishData.PizzaMargarita.id)
    recipe = Recipe.query.get(self.data.RecipeData.PizzaMargarita.id)
    self.assertNotIn(ingredient.id, [i.id for i in dish.ingredients])
    self.assertNotIn(ingredient.id, [i.id for i in recipe.ingredients])


  def test_non_admin_access(self):
    """
    Ensure regular users can't edit or create ingredients
    """
    login_URL = url_for('identities.login')

    create_URL = url_for('recipes.ingredients')
    response = self.login(ID=self.data.IdentityData.ActiveUser)
    response = self.client.get(create_URL)
    self.assertRedirects(response,
                         '%s?%s' % (login_URL, urlencode({"next": create_URL})))

    edit_URL = url_for('recipes.edit_ingredient',
                       id=self.data.IngredientData.Butter.id)
    response = self.login(ID=self.data.IdentityData.ActiveUser)
    response = self.client.get(edit_URL)
    self.assertRedirects(response,
                         '%s?%s' % (login_URL, urlencode({'next':edit_URL})))

  def test_create_ingredient(self):
    response = self.login(ID=self.data.IdentityData.Admin)

    response = self.client.get(url_for('recipes.ingredients'))
    self.assertStatus(response, 200)

    response = self.client.post(url_for('recipes.ingredients'),
                                data=self.create_data)

    # ensure record got created
    ingredient = Ingredient.query.filter_by(**self.create_data).first()

    self.assertRedirects(response,
                         url_for('recipes.edit_ingredient', id=ingredient.id))

  def test_create_unique_name(self):
    """
    Ensure can't create 2 ingredient with the same name
    """
    create_data = {"name": self.data.IngredientData.Ricotta.name}
    initial = Ingredient.query.filter_by(**create_data).count()

    response = self.login(ID=self.data.IdentityData.Admin)

    response = self.client.post(url_for('recipes.ingredients'),
                                data=create_data)
    self.assertStatus(response, 200)
    self.assertIn("already exist", response.data)

    # ensure no record got created
    self.assertEqual(Ingredient.query.filter_by(**create_data).count(), initial)

  def test_edit_ingredient(self):
    from_ingredient = self.data.IngredientData.Ricotta
    to_ingredient = self.data.IngredientData.Butter
    edit_url = url_for('recipes.edit_ingredient',
      id=self.data.IngredientData.Ricotta.id)

    response = self.login(ID=self.data.IdentityData.Admin)

    # ensure uniqueness
    response = self.client.post(edit_url,
                                data={"name": to_ingredient.name})
    self.assertIn("already exist", response.data)
    self.assertNotEqual(Ingredient.query.get(from_ingredient.id).name,
                        to_ingredient.name)

    # update ingredient
    response = self.client.post(edit_url, data=self.edit_data)
    self.assertRedirects(response, url_for('recipes.ingredients'))
    self.assertNotIn("already exist", response.data)

    # ensure ingredient got updated
    ingredient = Ingredient.query.filter_by(**self.edit_data).first()
    self.assertTrue(ingredient)
    self.assertEqual(ingredient.id, self.data.IngredientData.Ricotta.id)
    self.assertEqual(ingredient.name, self.edit_data['name'])

  def test_combined_ingredients(self):
    # ingredient cannot be it's own child
    ingredient = self.data.IngredientData2.Pasta
    edit_URL = url_for("recipes.edit_ingredient", id=ingredient.id)

    ingredient_ids = [parent.id for parent in ingredient.parents]
    ingredient_ids.append(self.data.IngredientData.Peperoni.id)
    ingredient_ids.append(self.data.IngredientData2.Pasta.id)

    edit_data = {
      "name": ingredient.name,
      "parents": ingredient_ids,
    }

    response = self.login(ID=self.data.IdentityData.Admin)
    response = self.client.post(edit_URL, data=edit_data)
    self.assertRedirects(response, url_for('recipes.ingredients'))

    new_ingredient = Ingredient.query.get(ingredient.id)
    self.assertEqual(len(new_ingredient.parents), 3)
    self.assertIn(new_ingredient.parents[0].id, ingredient_ids)
    self.assertIn(new_ingredient.parents[1].id, ingredient_ids)
    self.assertIn(new_ingredient.parents[2].id, ingredient_ids)

  def test_substitute_ingredient(self):
    substitute = self.data.IngredientData.Basil
    suggestion = self.data.IngredientData.Morglub

    dish_count = len(self.data.DishData.PizzaMargarita.ingredients)
    recipe_count = len(self.data.RecipeData.Lasagna.ingredients)

    recipe = Recipe.query.get(self.data.RecipeData.Lasagna.id)
    dish = Dish.query.get(self.data.DishData.PizzaMargarita.id)

    substitute_URL = url_for('recipes.ingredient_substitute', id=suggestion.id)
    # ensure only admin can access subtitute feature
    self.login(ID=self.data.IdentityData.ActiveUser)
    response = self.client.post(substitute_URL,
                                data={'ingredient': substitute.id})
    self.assertStatus(response, 302)
    self.assertPath(response, url_for('identities.login'))

    # Let substitute the suggestion with an ingredient already in the recipe
    # but not in the dish. recipe count should shrink, dish should keep the
    # same count
    self.login(ID=self.data.IdentityData.Admin)
    response = self.client.post(substitute_URL,
                                data={'ingredient': substitute.id})
    self.assertRedirects(response, url_for('recipes.edit_ingredient',
                                           id=substitute.id))

    recipe = Recipe.query.get(self.data.RecipeData.Lasagna.id)
    dish = Dish.query.get(self.data.DishData.PizzaMargarita.id)
    self.assertEqual(len(recipe.ingredients), recipe_count - 1)
    self.assertEqual(len(dish.ingredients), dish_count)

    # assert substitute ingredient got inserted
    self.assertIn(substitute.id, [i.id for i in dish.ingredients])
    self.assertIn(substitute.id, [i.id for i in recipe.ingredients])

    # assert suggested ingredient got removed
    self.assertNotIn(suggestion.id, [i.id for i in dish.ingredients])
    self.assertNotIn(suggestion.id, [i.id for i in recipe.ingredients])

    # suggestion ingredient should have been deleted
    self.assertNotExist(Ingredient, name=suggestion.name)
