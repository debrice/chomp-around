from urllib import urlencode

from flask import url_for

from app.lib.testing import TestCase
from app.identities.tests import fixtures as identity_fixtures
from app.restaurants.tests import fixtures as restaurant_fixtures
from app.restaurants.models import Dish

from .fixtures import all_data, env
from ..models import Recipe
from ..constants import ENTREE

env.update(identity_fixtures.env)
env.update(restaurant_fixtures.env)


class TestRecipe(TestCase):
  env = env
  datasets = all_data + identity_fixtures.all_data + \
    restaurant_fixtures.all_data

  create_data = {
    'name': 'kotteri',
  }

  edit_data = {
    'name': 'kotteri ramen',
    'category': ENTREE,
  }

  def test_non_admin_access(self):
    """
    Ensure regular users can't edit or create recipes
    """
    login_URL = url_for('identities.login')

    create_URL = url_for('recipes.recipes')
    response = self.login(ID=self.data.IdentityData.ActiveUser)
    response = self.client.get(create_URL)
    self.assertRedirects(response,
                         '%s?%s' % (login_URL, urlencode({"next": create_URL})))

    edit_URL = url_for('recipes.edit_recipe',
                       id=self.data.RecipeData.Lasagna.id)
    response = self.login(ID=self.data.IdentityData.ActiveUser)
    response = self.client.get(edit_URL)
    self.assertRedirects(response,
                         '%s?%s' % (login_URL, urlencode({'next':edit_URL})))

  def test_create_recipe(self):
    response = self.login(ID=self.data.IdentityData.Admin)

    response = self.client.get(url_for('recipes.recipes'))
    self.assertStatus(response, 200)

    response = self.client.post(url_for('recipes.recipes'),
      data=self.create_data)

    # ensure recipe got created
    self.assertExist(Recipe, **self.create_data)
    recipe = self.last(Recipe)
    self.assertRedirects(response, url_for('recipes.edit_recipe', id=recipe.id))

  def test_create_unique_name(self):
    """
    Ensure can't create 2 recipe with the same name
    """
    create_data = {"name": self.data.RecipeData.Lasagna.name}
    initial = Recipe.query.filter_by(**create_data).count()

    response = self.login(ID=self.data.IdentityData.Admin)

    response = self.client.post(url_for('recipes.recipes'), data=create_data)
    self.assertStatus(response, 200)
    self.assertIn("already exist", response.data)

    # ensure not record got created
    self.assertEqual(Recipe.query.filter_by(**create_data).count(), initial)

  def test_edit_recipe(self):
    edit_url = url_for('recipes.edit_recipe',
      id=self.data.RecipeData.Lasagna.id)

    response = self.login(ID=self.data.IdentityData.Admin)

    # ensure uniqueness
    response = self.client.post(edit_url,
                         data={"name": self.data.RecipeData.PizzaMargarita.name})
    self.assertIn("already exist", response.data)
    self.assertNotEqual(Recipe.query.get(self.data.RecipeData.Lasagna.id).name,
      self.data.RecipeData.PizzaMargarita.name)

    # update a recipe
    response = self.client.post(edit_url, data=self.edit_data)
    self.assertRedirects(response, url_for('recipes.recipes'))
    self.assertNotIn("already exist", response.data)

    # ensure recipe got updated
    recipe = Recipe.query.get(self.data.RecipeData.Lasagna.id)
    self.assertTrue(recipe)
    self.assertEqual(recipe.id, self.data.RecipeData.Lasagna.id)
    self.assertEqual(recipe.name, self.edit_data['name'])

  def test_substitute_recipe(self):
    substitute = self.data.RecipeData.Lasagna
    suggestion = self.data.RecipeData.Morglub

    substitute_URL = url_for('recipes.recipe_substitute', id=suggestion.id)
    # ensure only admin can access subtitute feature
    self.login(ID=self.data.IdentityData.ActiveUser)
    response = self.client.post(substitute_URL,
                                data={'recipe': substitute.id})
    self.assertStatus(response, 302)
    self.assertPath(response, url_for('identities.login'))

    self.login(ID=self.data.IdentityData.Admin)
    response = self.client.post(substitute_URL,
                                data={'recipe': substitute.id})
    self.assertRedirects(response, url_for('recipes.edit_recipe',
                                           id=substitute.id))

    dish = Dish.query.get(self.data.DishData.Morglub.id)
    self.assertEqual(dish.recipe_id, substitute.id)

    # suggestion ingredient should have been deleted
    self.assertNotExist(Recipe, name=suggestion.name)
