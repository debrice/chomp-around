from fixture import DataSet

from app.recipes.models import Recipe, Ingredient, Tag, IngredientCategory

from ..constants import ENTREE, APPETIZER, SUGGESTION, VALID


class IngredientData(DataSet):
  class Butter:
    name = "butter"
    status_id = VALID

  class Milk:
    name = "milk"
    status_id = VALID

  class Mozarella:
    name = "mozarella"
    status_id = VALID

  class Egg:
    name = "egg"
    status_id = VALID

  class Flour:
    name = "flour"
    status_id = VALID

  class Tomatoe:
    name = "tomatoe"
    status_id = VALID

  class Basil:
    name = "basil"
    status_id = VALID

  class Ricotta:
    name = "ricotta"
    status_id = VALID

  class Parmigiano:
    name = "parmigiano"
    status_id = VALID

  class Oregano:
    name = "oregano"
    status_id = VALID

  class Peperoni:
    name = "peperoni"
    status_id = VALID

  class Salt:
    name = "salt"
    status_id = VALID

  class Morglub:
    name = "morglub"
    status_id = SUGGESTION


class IngredientData2(DataSet):
  class Pasta:
    name = "pasta"
    parents = [IngredientData.Egg, IngredientData.Flour]
    status_id = VALID

  class PizzaDoe:
    name = "pizza doe"
    parents = [IngredientData.Flour, IngredientData.Salt]
    status_id = VALID


class RecipeData(DataSet):
  class Lasagna:
    category_id = ENTREE
    name = "lasagna"
    status_id = VALID
    ingredients = [IngredientData2.Pasta, IngredientData.Tomatoe,
      IngredientData.Morglub, # suggested ingredient
      IngredientData.Basil, IngredientData.Ricotta, IngredientData.Parmigiano]

  class PizzaMargarita:
    category_id = APPETIZER
    name = "pizza margarita"
    status_id = VALID
    ingredients = [IngredientData2.PizzaDoe, IngredientData.Mozarella,
      IngredientData.Oregano, IngredientData.Tomatoe]

  class Morglub:
    category_id = APPETIZER
    name = "Morglub"
    status_id = SUGGESTION
    ingredients = [IngredientData.Mozarella, IngredientData.Tomatoe,
      IngredientData.Basil]


class IngredientCategoryData(DataSet):
  class Dairy:
    name = "dairy"
    ingredients = [IngredientData.Ricotta, IngredientData.Milk,
      IngredientData.Mozarella, IngredientData.Parmigiano]

  class Cheese:
    name = 'cheese'
    ingredients = [IngredientData.Ricotta, IngredientData.Mozarella,
      IngredientData.Parmigiano]


class TagData(DataSet):
  class Italian:
    name = "italian"
    recipes = [RecipeData.PizzaMargarita, RecipeData.Lasagna]

  class Pizza:
    name = "pizza"
    recipes = [RecipeData.PizzaMargarita]


all_data = [
  IngredientData,
  IngredientData2,
  RecipeData,
  IngredientCategoryData,
  TagData]


env = {
  "IngredientData": Ingredient,
  "IngredientData2": Ingredient,
  "RecipeData": Recipe,
  "IngredientCategoryData": IngredientCategory,
  "TagData": Tag,
}
