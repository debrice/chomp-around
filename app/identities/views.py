from werkzeug import check_password_hash, generate_password_hash
from flask import Blueprint, request, render_template, flash, redirect, url_for
from flask.ext.login import login_user, login_required, current_user, \
  logout_user, fresh_login_required

from app import db

from forms import RegisterForm, LoginForm, ProfileForm, EmailForm, PasswordForm
from models import User, Identity

from app.restaurants.forms import RestaurantForm
from app.restaurants.views import _create_restaurant


mod = Blueprint('identities', __name__, url_prefix='/identities')


@mod.context_processor
def inject_login_form():
  return {'login_form': LoginForm()}


@mod.route('/settings/', methods=['GET', 'POST'])
@fresh_login_required
def settings():
  profile_form = ProfileForm(obj=current_user.user)
  email_form = EmailForm(obj=current_user)
  password_form = PasswordForm()
  if 'new_email' in request.form and email_form.validate_on_submit():
    current_user.email = email_form.new_email.data
    db.session.commit()
    flash('Your email address has been successfully updated.', 'success')
    return redirect(url_for('identities.settings'))

  elif 'password' in request.form and password_form.validate_on_submit():
    current_user.password = generate_password_hash(password_form.password.data)
    db.session.commit()
    flash('Your password has been successfully updated.', 'success')
    return redirect(url_for('identities.settings'))

  elif 'name' in request.form and profile_form.validate_on_submit:
    current_user.user.name = profile_form.name.data
    db.session.commit()
    flash('Your name has been successfully updated.', 'success')
    return redirect(url_for('identities.settings'))

  return render_template("identities/settings.html", identity=current_user,
    profile_form=profile_form, email_form=email_form,
    password_form=password_form, section='profile')


@mod.route('/me/')
@login_required
def home():
  return render_template("identities/profile.html", identity=current_user)


@mod.route('/register/restaurant/', methods=['GET', 'POST'])
def registerRestaurant():
  """
  register a user and a restaurant
  """
  form = RegisterForm()
  restaurant_form = RestaurantForm(prefix="restaurant")
  if form.validate_on_submit() and restaurant_form.validate_on_submit():
    user = _createUser(name=form.name.data, email=form.email.data,
      password=form.password.data)

    restaurant = _create_restaurant(restaurant_form.name.data, user=user,
      category_id=restaurant_form.category_id.data)
    db.session.commit()

    login_user(user.identity)

    # flash will display a message to the user
    flash('Thanks for registering', 'success')
    # redirect user to the settings of the new created restaurant
    return redirect(url_for('restaurants.settings', id=restaurant.id))
  return render_template("identities/register_restaurant.html", form=form,
    restaurant_form=restaurant_form)

def _createUser(name, email, password):
    # create an user instance not yet stored in the database
    email = email.lower()
    identity = Identity(email=email, password=generate_password_hash(password))
    db.session.add(identity)

    user = User(name=name, identity=identity)
    # Insert the record in our database and commit it
    db.session.add(user)
    return user

@mod.route('/register/', methods=['GET', 'POST'])
def register():
  """
  Registration Form
  """
  form = RegisterForm()
  if form.validate_on_submit():
    user = _createUser(name=form.name.data, email=form.email.data, \
      password=form.password.data)

    db.session.commit()

    login_user(user.identity)

    # flash will display a message to the user
    flash('Thanks for registering', 'success')
    # redirect user to the 'home' method of the user module.
    return redirect(url_for('identities.home'))
  return render_template("identities/register.html", form=form)

@mod.route('/logout/', methods=['GET'])
@login_required
def logout():
  """
  Logout current user
  """
  logout_user()
  return redirect(url_for('identities.login'))


@mod.route('/login/', methods=['GET', 'POST'])
def login():
  """
  Login form
  """
  form = LoginForm()
  # make sure data are valid, but doesn't validate password is right
  if form.validate_on_submit():
    identity = Identity.query.filter_by(email=form.email.data).first()
    # we use werzeug to validate user's password
    if identity and check_password_hash(identity.password, form.password.data):
      if login_user(identity, remember=form.remember.data):
        return redirect(request.args.get("next") or url_for('identities.home'))
    flash('Wrong email or password', 'error')
  return render_template("identities/login.html", form=form)
