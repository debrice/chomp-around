from sqlalchemy.ext.associationproxy import association_proxy

from app import db

import constants as IDENTITY

from app.images.models import Image


class Identity(db.Model):
  __tablename__ = 'identities_identity'

  id = db.Column(db.Integer, primary_key=True)
  role = db.Column(db.SmallInteger, default=IDENTITY.USER)
  status_id = db.Column(db.SmallInteger, default=IDENTITY.NEW)
  email = db.Column(db.String(255), unique=True)
  password = db.Column(db.String(20))

  @property
  def user(self):
    return self.users[0]

  def is_authenticated(self):
    return True

  def is_active(self):
    return self.status_id != IDENTITY.INACTIVE

  def is_anonymous(self):
    return False

  def is_admin(self):
    return self.role == IDENTITY.ADMIN

  def get_id(self):
    return unicode(self.id)

  @property
  def status(self):
    return IDENTITY.STATUS[self.status_id]

  def getRole(self):
    return IDENTITY.ROLE[self.role]


class User(db.Model):
  __tablename__ = 'identities_user'

  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(64), unique=True)
  identity_id = db.Column(db.Integer, db.ForeignKey('identities_identity.id'))
  image_id = db.Column(db.Integer)
  identity = db.relationship('Identity', backref='users')
  restaurants = association_proxy('roles', 'restaurant')

  @property
  def avatar_url(self):
    if self.image_id:
      image = Image.query.get(self.image_id)
      if image:
        return image.url
    return ""

class TokenAuth(db.Model):
  __tablename__ = 'identities_token_auth'

  id = db.Column(db.Integer, primary_key=True)
  token = db.Column(db.String(128), unique=True)
  identity_id = db.Column(db.Integer, db.ForeignKey('identities_identity.id'))
  identity = db.relationship('Identity', backref=db.backref('tokens', lazy='dynamic'))
