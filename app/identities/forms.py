from werkzeug import check_password_hash

from flask.ext.login import current_user
from flask.ext.wtf import Form, TextField, PasswordField, BooleanField, \
  Required, Email, EqualTo, ValidationError, Length, HiddenField

from .models import Identity


class LoginForm(Form):
  email = TextField('Email address', [Required(), Email(), Length(max=255)])
  password = PasswordField('Password', [Required()])
  remember = BooleanField('Remember me')
  next = HiddenField()


class RegisterForm(Form):
  name = TextField('Name', [Required(), Length(max=64)])
  email = TextField('Email address', [Required(), Email(), Length(max=255)])
  password = PasswordField('Password', [Required()])
  confirm = PasswordField('Repeat Password', [
      Required(),
      EqualTo('password', message='Passwords must match')
      ])

  def validate_email(self, field):
    if Identity.query.filter_by(email=field.data).first():
      raise ValidationError('An account with this email already exists.')


class ProfileForm(Form):
  name = TextField('Name', [Required(), Length(max=64)])


class EmailForm(Form):
  current_password = PasswordField('Current Password', [Required()])
  new_email = TextField('Email address', [Required(), Email(), Length(max=255)])

  def validate_current_password(self, field):
    if not check_password_hash(current_user.password, field.data):
      raise ValidationError('Wrong password.')


class PasswordForm(Form):
  current_password = PasswordField('Current Password', [Required()])
  password = PasswordField('Password', [Required()])
  confirm = PasswordField('Repeat Password', [
      Required(),
      EqualTo('password', message='Passwords must match')
      ])

  def validate_current_password(self, field):
    if not check_password_hash(current_user.password, field.data):
      raise ValidationError('Wrong password.')

