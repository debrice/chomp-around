import unittest
from flask import url_for

from app.lib.testing import TestCase
from app.identities.models import Identity
from app.restaurants import constants as RESTAURANT
from app.restaurants.models import Restaurant, Role

from fixtures import all_data, env

class TestAuth(TestCase):
  datasets = all_data
  env = env
  reg_email = "example@example.com"
  # additional data
  reg_data = {
    "email": reg_email,
    "name": "john",
    "password": "password",
    "confirm": "password",
  }

  reg_restaurant_data = {
    "email": reg_email,
    "name": "boris",
    "password": "password",
    "confirm": "password",
    # restaurant form is distinguished by "restaurant-" prefix
    "restaurant-name": "sweet ice cream",
    "restaurant-category_id": RESTAURANT.RESTAURANT,
  }

  def test_RegisterPageContainsTOSLink(self):
    response = self.client.get(url_for("identities.register"))
    self.assertStatus(response, 200)
    self.assertIn('<a href="/about/tos/">terms of service</a>', response.data)

  def test_register_restaurant(self):
    reg_URL = url_for("identities.registerRestaurant")

    # simple rendering, ensure template exists, and render without exception
    rv = self.client.get(reg_URL)
    self.assertStatus(rv, 200)

    # register a new restaurant should redirect to the dashboard
    rv = self.client.post(reg_URL, data=self.reg_restaurant_data)

    # ensure identity got created
    identity = Identity.query.filter_by(email=self.reg_email).first()
    self.assertTrue(identity)

    restaurant = Restaurant.query.filter_by(
      name=self.reg_restaurant_data["restaurant-name"]).first()
    self.assertTrue(restaurant)

    # creation should redirect to settings page of the created restaurant
    self.assertRedirects(rv, url_for("restaurants.settings",
                                           id=restaurant.id))

    # User should be an admin of the created restaurant
    self.assertExist(Role, user_id=identity.user.id, role_id=RESTAURANT.OWNER,
      restaurant_id=restaurant.id)


  def test_registerExistingEmail(self):
    reg_data = self.reg_data.copy()
    reg_data["email"] = self.data.IdentityData.InactiveUser.email
    response = self.client.post(url_for("identities.register"), data=reg_data)
    self.assertStatus(response, 200)
    self.assertIn("email already exists", response.data)

  def test_emailRegister(self):
    response = self.client.post(url_for("identities.register"),
                                data=self.reg_data)
    self.assertStatus(response, 302)
    self.assertExist(Identity, email=self.reg_data["email"])

  def test_inactiveLogin(self):
    response = self.client.get(url_for("identities.login"))
    self.assertStatus(response, 200)
    login_data = {
      "email": self.data.IdentityData.InactiveUser.email,
      "password": "password",
    }
    response = self.client.post(url_for("identities.login"), data=login_data)
    self.assertStatus(response, 200);
    self.assertIn("Wrong email or password", response.data)

  def test_login(self):
    response = self.client.get(url_for("identities.login"))
    self.assertStatus(response, 200)
    login_data = {
      "email": self.data.IdentityData.ActiveUser.email,
      "password": "password",
    }
    response = self.client.post(url_for("identities.login"), data=login_data)
    self.assertStatus(response, 302);
    self.assertRedirects(response, url_for("identities.home"))
    response = self.client.get(url_for("identities.home"))
    self.assertStatus(response, 200);

def suite():
  suite = unittest.TestSuite()
  suite.addTest(unittest.makeSuite(TestAuth))
  return suite
