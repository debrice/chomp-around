from fixture import DataSet
from werkzeug import generate_password_hash

from ..models import User, Identity
from ..constants import ADMIN, ACTIVE, USER, NEW, INACTIVE


class IdentityData(DataSet):
  class Admin:
    role = ADMIN
    status_id = ACTIVE
    email = "admin@example.com"
    password = generate_password_hash("password")

  class NewUser:
    role = USER
    status_id = NEW
    email = "roger@example.com"
    password = generate_password_hash("password")

  class ActiveUser:
    role = USER
    status_id = ACTIVE
    email = "steve@example.com"
    password = generate_password_hash("password")

  class OtherActiveUser:
    role = USER
    status_id = ACTIVE
    email = "other@example.com"
    password = generate_password_hash("password")

  class InactiveUser:
    role = USER
    status_id = INACTIVE
    email = "robert@example.com"
    password = generate_password_hash("password")


class UserData(DataSet):
  class Robert:
    name = "Robert"
    identity = IdentityData.InactiveUser

  class Roger:
    name = "Roger"
    identity = IdentityData.NewUser

  class Steve:
    name = "Steve"
    identity = IdentityData.ActiveUser

  class Chris:
    name = "Chris"
    identity = IdentityData.OtherActiveUser

  class Anthony:
    name = "Anthony"
    identity = IdentityData.Admin

  class Mike:
    name = "Mike"
    identity = IdentityData.Admin


all_data = [IdentityData, UserData]

env = {
  "UserData": User,
  "IdentityData": Identity,
}
