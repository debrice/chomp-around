import unittest

from flask import url_for
from werkzeug import check_password_hash

from app.lib.testing import TestCase
from app.identities.models import Identity, User

from fixtures import all_data, env


class TestSettings(TestCase):
  datasets = all_data
  env = env

  def set_up(self):
    self.identity = self.data.IdentityData.ActiveUser


  def test_settings(self):
    self.login(ID=self.identity)
    response = self.client.get(url_for("identities.settings"))
    # get on settings should display without errors
    self.assertStatus(response, 200)

  def test_updatePasswordRequiresOldPassword(self):
    update_data = {
      "current_password": "wrong password",
      "password": "new password",
      "new_password": "new password",
    }
    self.login(ID=self.identity)
    response = self.client.post(url_for("identities.settings"),
                                data=update_data)

    # No redirect on failure
    self.assertStatus(response, 200)

    # Ensure warning is displayed
    self.assertIn("Wrong password", response.data)

    # Current password should remain the same
    identity = Identity.query.filter_by(email=self.identity.email).first()
    self.assertEqual(self.identity.password, identity.password)


  def test_updateEmailRequiresCurrentPassword(self):
    update_data = {
      "current_password": "wrong password",
      "new_email": "new@example.com",
    }
    self.login(ID=self.identity)
    response = self.client.post(url_for("identities.settings"),
                                data=update_data)

    # No redirect on failure
    self.assertStatus(response, 200)

    # Ensure success message is displayed
    self.assertIn("Wrong password", response.data)

    # Current email should remain the same
    self.assertExist(Identity, email=self.identity.email)


  def test_updatePassword(self):
    update_data = {
      "current_password": "password",
      "password": "new password",
      "confirm": "new password",
    }
    self.login(ID=self.identity)
    response = self.client.post(url_for("identities.settings"),
                                data=update_data)

    # redirect on success
    self.assertStatus(response, 302)
    self.assertRedirects(response, url_for("identities.settings"))
    response = self.client.get(url_for("identities.settings"))
    self.assertStatus(response, 200);

    # Ensure success message is displayed
    self.assertIn("successfully updated", response.data)

    # Current password should have been updated
    identity = Identity.query.get(self.identity.id)
    self.assertTrue(check_password_hash(identity.password,
                                        update_data["password"]))


  def test_updateEmail(self):
    update_data = {
      "current_password": "password",
      "new_email": "new@example.com",
    }
    self.login(ID=self.identity)
    response = self.client.post(url_for("identities.settings"),
                                data=update_data)

    # redirect on success
    self.assertStatus(response, 302)
    self.assertRedirects(response, url_for("identities.settings"))
    response = self.client.get(url_for("identities.settings"))
    self.assertStatus(response, 200);

    # Ensure success message is displayed
    self.assertIn("successfully updated", response.data)
    # current email should have changed
    self.assertExist(Identity, email=update_data["new_email"])


  def test_updateName(self):
    update_data = {
      "name": "new name",
    }
    self.login(ID=self.identity)
    response = self.client.post(url_for("identities.settings"),
                                data=update_data)

    # Redirect on success
    self.assertStatus(response, 302)
    self.assertRedirects(response, url_for("identities.settings"))
    response = self.client.get(url_for("identities.settings"))
    self.assertStatus(response, 200)

    # Ensure success message is displayed
    self.assertIn("successfully updated", response.data)

    # Current name should have changed
    user = User.query.get(self.identity.user.id)
    self.assertEqual(user.name, update_data["name"])


def suite():
  suite = unittest.TestSuite()
  suite.addTest(unittest.makeSuite(TestSettings))
  return suite
