from app import login_manager

from .models import Identity


@login_manager.user_loader
def load_user(identity_id):
    return Identity.query.get(identity_id)

