from functools import wraps

from flask import current_app
from flask.ext.login import current_user


def admin_required(fn):
  """Requires admin credentials"""
  @wraps(fn)
  def decorated_view(*args, **kwargs):
    if not current_user.is_authenticated() or not current_user.is_admin():
      return current_app.login_manager.unauthorized()
    return fn(*args, **kwargs)
  return decorated_view

