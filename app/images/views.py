import os
from PIL import Image as Im

from flask import Blueprint, request, render_template, flash, json, url_for, \
  redirect, abort
from flask.ext.login import login_required, current_user

from app import photos, db
from app.lib.decorators import remote

from .forms import ImageUploadForm, ImageCropForm
from .models import Image, TempImage


mod = Blueprint("images", __name__, url_prefix="/images")


@mod.route("/album/<album>/", methods=["GET"])
@login_required
def album(album):
  images = Image.query.filter_by(owner=current_user, album=album)
  return render_template('images/album.html', images=images)


@mod.route("/album/<album>/upload/", methods=["GET","POST"])
@login_required
def upload(album):
  form = ImageUploadForm()
  if form.validate_on_submit():
    image = Image()
    image.owner = current_user.user
    image.album = album
    image.file = photos.save(request.files["image"])

    db.session.add(image)
    db.session.commit();
    flash("Image successfully uploaded", "success")
    return redirect(url_for('images.album', album=image.album))
  else:
    flash("An error occured: %s" % ",".join(form.errors), "error")

  return render_template("images/upload.html", form=form)


@mod.route("/r/album/<album>/picker/", methods=["GET","POST"])
@login_required
def remote_image_picker(album):
  form = ImageUploadForm()
  return render_template("images/picker.html", form=form, album=album)


@mod.route("/r/album/<album>/upload/", methods=["GET","POST"])
@login_required
def image_upload(album):
  form = ImageUploadForm()
  return render_template("images/picker.html", form=form)


@mod.route("/r/save/", methods=["POST"])
@remote
@login_required
def save():
  """
  Save and crop the temporary image
  """
  temp_image = TempImage.query.get_or_404(request.args.get('id'))
  if temp_image.owner_id != current_user.user.id:
    abort(404)

  form = ImageCropForm(csrf_enabled=False)
  if form.validate_on_submit():
    image = Image()
    im = Im.open(temp_image.path)
    region = im.crop((
      form.from_x.data,
      form.from_y.data,
      form.from_x.data + form.side.data,
      form.from_y.data + form.side.data,
    ))

    dirname, filename = os.path.split(temp_image.path)
    filename, ext = os.path.splitext(filename)
    region.save(os.path.join(dirname , filename + '_cropped_' + ext))
    image.file = filename + '_cropped_' + ext
    image.owner = temp_image.owner
    image.album = temp_image.album
    image.size = form.side.data

    db.session.delete(temp_image)
    db.session.add(image)
    db.session.commit()

    return {
      'url': image.url,
      'id': image.id,
      'album': image.album,
    }

  return {
    'error': form.errors
  }


@mod.route("/album/<album>/upload_iframe/", methods=["GET","POST"])
@login_required
def upload_iframe(album):
  """
  Stores the uploaded image as a temp image.
  Sucess:
  Calls JS CA.uploads.success callback on the parent window
  Error:
  Calls JS CA.uploads.errors callback on the parent window
  """
  form = ImageUploadForm()
  if form.validate_on_submit():
    image = TempImage()
    image.owner = current_user.user
    image.album = album
    image.file = photos.save(request.files["image_upload"])

    db.session.add(image)
    db.session.commit();
    script = "window.parent.CA.uploads.success(%s);" % (
      json.dumps({'url': image.url,
                  'id': image.id,
                  'album': image.album})
    )
  else:
    script = "window.parent.CA.uploads.errors(%s);" % json.dumps(form.errors)

  return render_template("minimal.html", script=script)


@mod.route("/<int:id>/delete/", methods=["POST"])
def delete_image(id):
  image = Image.query.filter_by(id=id, owner=current_user.user).first_or_404()
  db.session.delete(image)
  db.session.commit()

  return render_template("minimal.html")


@mod.route("/r/album/", methods=["GET"])
@mod.route("/r/album/<album>/", methods=["GET"])
@login_required
def remote_album(album=None):
  kwargs = {'owner': current_user.user}

  if album:
    kwargs['album'] = album

  images = Image.query.filter_by(**kwargs).order_by(db.desc(Image.id))

  return render_template('images/picker_album.html', images=images)
