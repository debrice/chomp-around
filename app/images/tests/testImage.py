import os
from PIL import Image as Im

from flask import url_for

from app.identities.tests import fixtures
from app.lib.testing import TestCase

from ..models import TempImage, Image

class TestImage(TestCase):

  datasets = fixtures.all_data
  env = fixtures.env

  def set_up(self):
    self.identity = self.data.IdentityData.ActiveUser
    self.root = os.path.dirname(os.path.abspath(__file__))
    self.album = 'test_album'

  def upload_temp_image(self, image='image.png'):
    upload_temp_URL = url_for('images.upload_iframe', album=self.album)
    file_name = os.path.join(self.root, 'image.png')
    return self.client.post(upload_temp_URL,
                            data={'image_upload':open(file_name)})

  def crop_image(self, image):
    save_data = {
      'from_x': 100,
      'from_y': 100,
      'side': 200,
    }
    upload_path = url_for("images.save") + '?id=%s' % image.id
    return self.client.post(data=save_data, path=upload_path)

  def test_iframe_upload_to_album(self):
    self.login(ID=self.identity)
    rv = self.upload_temp_image()
    self.assertStatus(rv, 200)

    # find image, validate data are recorded
    temp_image = TempImage.query.first()
    self.assertTrue(temp_image)
    self.assertTrue(os.path.exists(temp_image.path))
    self.assertTrue(temp_image.album == self.album)

    # delete the image manually
    os.remove(temp_image.path)

  def test_save_cropped_image(self):
    self.login(ID=self.identity)
    self.upload_temp_image()
    temp_image = TempImage.query.first()
    self.assertTrue(temp_image)
    rv = self.crop_image(temp_image)
    self.assertStatus(rv, 200)

    image = Image.query.first()
    # assert image values are correct
    self.assertTrue(image)
    self.assertTrue(os.path.exists(image.path))
    self.assertTrue(image.album == temp_image.album)
    self.assertTrue(image.owner_id == image.owner_id)

    # assert image got cropped
    im = Im.open(image.path)
    self.assertTrue(im.size[0] == im.size[1])
    self.assertTrue(im.size[0] == image.size)

    # ensure temp file got deleted
    self.assertFalse(TempImage.query.get(temp_image.id))
    self.assertFalse(os.path.exists(temp_image.path))

    os.remove(image.path)

  def test_cannot_delete_image(self):
    """
    Ensure user X cannot delete user Y's image
    """
    self.login(ID=self.identity)
    self.upload_temp_image()
    temp_image = TempImage.query.first()
    self.crop_image(temp_image)

    image = Image.query.first()
    self.assertTrue(image)

    self.assertTrue(os.path.exists(image.path))
    self.login(ID=self.data.IdentityData.OtherActiveUser)
    rv = self.client.post(url_for("images.delete_image", id=image.id))
    self.assertStatus(rv, 404)
    self.assertTrue(os.path.exists(image.path))
    os.remove(image.path)

  def test_delete_image(self):
    """
    Creates an image, ensures it gets deleted
    """
    self.login(ID=self.identity)
    self.upload_temp_image()
    temp_image = TempImage.query.first()
    self.crop_image(temp_image)

    image = Image.query.first()
    self.assertTrue(image)
    self.assertTrue(os.path.exists(image.path))
    self.login(ID=self.identity)
    rv = self.client.post(url_for("images.delete_image", id=image.id))
    self.assertStatus(rv, 200)
    self.assertFalse(os.path.exists(image.path))
