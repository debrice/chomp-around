from flask.ext.wtf import Form, file_allowed, file_required, FileField, \
  IntegerField, NumberRange

from app import photos


class ImageUploadForm(Form):
  image_upload = FileField("Upload your image",
                    validators=[file_required(),
                                file_allowed(photos, "Images only!")])


class ImageCropForm(Form):
  from_x = IntegerField(NumberRange(min=0))
  from_y = IntegerField(NumberRange(min=0))
  side = IntegerField(NumberRange(min=100))
