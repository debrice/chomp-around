import os

from datetime import datetime

from sqlalchemy import event

from flask.ext.login import current_user
from app import db, photos


class TempImage(db.Model):
  __tablename__ = "images_tempimage"

  id = db.Column(db.Integer, primary_key=True)
  file = db.Column(db.Text())
  album = db.Column(db.String(255))
  owner_id = db.Column(db.Integer,
                 db.ForeignKey("identities_user.id"))
  owner = db.relationship("User")
  creation_date = db.Column(db.DateTime, default=datetime.utcnow)

  @property
  def url(self):
    return photos.url(self.file)

  @property
  def path(self):
    return photos.path(self.file)


def after_delete_temp_image(mapper, connection, target):
  os.remove(target.path)

event.listen(TempImage, "after_delete", after_delete_temp_image)


class Image(db.Model):
  __tablename__ = "images_image"

  id = db.Column(db.Integer, primary_key=True)
  file = db.Column(db.Text())
  size = db.Column(db.Integer)
  album = db.Column(db.String(255))
  creation_date = db.Column(db.DateTime, default=datetime.utcnow)
  owner_id = db.Column(db.Integer,
                 db.ForeignKey("identities_user.id"))
  owner = db.relationship("User")

  @property
  def url(self):
    return photos.url(self.file)

  @property
  def path(self):
    return photos.path(self.file)


def after_delete_image(mapper, connection, target):
  os.remove(target.path)

event.listen(Image, "after_delete", after_delete_image)


def get_temp_images():
  #TODO: limit to the places within the current restaurant
  return TempImage.query.filter_by(owner=current_user.user)


def get_images():
  #TODO: limit to the places within the current restaurant
  return Image.query.filter_by(owner=current_user.user)
