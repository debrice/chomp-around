from flask import Flask, render_template
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager
from flaskext.uploads import IMAGES, UploadSet, configure_uploads

from lib import register_filters

# Database
db = SQLAlchemy()
login_manager = LoginManager()

# uploads
photos = UploadSet("photos", IMAGES)

app = None

def create_app(config="config"):
  # Init app and load config
  global app;
  app = Flask(__name__)
  app.config.from_object(config)

  # Login manager
  login_manager.setup_app(app)
  login_manager.login_view = "identities.login"
  login_manager.login_message = u"Login required."
  login_manager.refresh_view = "identities.login"
  login_manager.needs_refresh_message = (
      u"To protect your account, please reauthenticate to access this page."
      )

  # Error Handler
  @app.errorhandler(404)
  def not_found(error):
      return render_template("404.html"), 404

  # Importing blueprint
  from app.identities.views import mod as identitiesModule
  from app.images.views import mod as imagesModule
  from app.recipes.views import mod as recipesModule
  from app.restaurants.views import mod as restaurantsModule
  #from app.comments.views import mod as commentsModule
  #from app.ratings.views import mod as ratingsModule

  # Registering blueprint
  app.register_blueprint(identitiesModule)
  app.register_blueprint(imagesModule)
  app.register_blueprint(recipesModule)
  app.register_blueprint(restaurantsModule)
  #app.register_blueprint(commentsModule)
  #app.register_blueprint(ratingsModule)

  configure_uploads(app, photos)

  # Initialize SQLAlchemy
  db.init_app(app)
  register_filters(app)
  return app
