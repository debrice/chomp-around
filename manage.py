#!/usr/bin/env python

from flask.ext.script import Manager, Server, Shell

from app import create_app, db

app = create_app()
app.debug = True

manager = Manager(app)

manager.add_command("runserver", Server())
manager.add_command("shell", Shell())

@manager.command
def import_ingredients():
  """
  import ingredients in the current db
  """
  from app.bin.import_ingredients import run
  print "Starting import of ingredients in the database"
  run()
  print "Done"

@manager.command
def create_tables():
  """
  create all the tables
  """
  db.create_all()

@manager.command
def make_admin(user_id):
  """
  Make user with id == user_id an admin
  """
  from app.identities.models import Identity
  from app.identities import constants as IDENTITY

  if user_id is None:
    print "ERROR: missing user_id argument"
    return

  identity = Identity.query.get(user_id)

  if identity is None:
    print "ERROR: Can not find user with ID %s" % user_id
    return

  if identity.role == IDENTITY.ADMIN:
    print "ERROR: User %s is already an admin" % user_id
    return

  identity.role = IDENTITY.ADMIN
  db.session.commit()
  print "User %s is now an admin" % user_id

@manager.command
def generate_data():
  """
  Generate random data in the DB
  """
  from app.bin.generate_data import run
  run()


if __name__ == "__main__":
  manager.run()
