from config import *

TESTING = True
SQLALCHEMY_DATABASE_URI = "sqlite://"
CSRF_ENABLED = False
